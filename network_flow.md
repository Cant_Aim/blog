---
title: Network Flow and Linear Programming (Part One)
taxonomy:
    category: blog
    tag: [graph theory, complexity, analysis, flow, cuts, algorithms, optimization, ford-fulkerson, edmonds-karp, bipartite matching]
---

In this article we will explore network flow algorithms, specifically we define network flow, talk about max flow min cut theorem, explore the surprising applications of the theorem in domains that may seem unrelated, and finally talk about it's relationship to the topic of linear programming.

We will also spend some time discussing technical details of linear programming itself. We will give an overview convexity and then delve into deeper topics like duality and degeneracy. We will also provide some details into how simplex method is able to solve problem of finding an optimal point on a polyhedron efficiently.

This article here is based on my notes from graduate studies at _University of Connecticut_ and the online _Advanced Algorithms_ course that is taught by the _University of California San Diego & National Research University Higher School of Economics_. Thus, we are assuming a decent familiarity with sorting and graph algorithms. If you have no exposure to undergraduate computer science courses I recommend coming back to this _only_ after you have taken some.

===

## Definition of Max Flow Problem

When talking about flows we assume the following:

* A directed graph $G=(V,E)$.
* Each edge has a total flow $f_e$.
* Each edge has a capacity flow $f_c$.
* Conservation of flow, $\sum_{in}f_e = \sum_{out}f_e$ where $in$ and $out$ is flow going into and out of a node $v\in V$ respectively.
* Rate limitation $0\leq f_e \leq c_e$.
* Flow size is $\lvert f \rvert = \sum_{in}f_e-\sum_{out}f_e$ where $in$ and $out$ is flow going into and out of $source$ node respectively. This is representative of the flow going from source to sink.

Given the above we wish to find assignments of $f_e$ for $\forall e\in E$ $s.t$ $\lvert f \rvert$ is maximized.

## Solving for Max Flow

Now that we formally stated the problem we wish to solve the next step is to discuss how we will solve it. Before diving into the exact algorithms that will give us solutions for arbitrary graphs we need to understand a number of _theorems_ and _concepts_ that the algorithms will lean on to be able to solve the problem.

### Residual Networks

Given a directed graph $G$ we are able to construct a _residual network_ that we will refer to $G_f$. The point of $G_f$ is that it tells us where flow can still be added and where _existing flow can be canceled out_.

The residual graph $G_f$ is defined same as $G$ except that it has an opposite edge $e_o$ for $\forall e \in E$. The rules for constructing the graph are as follows. $\forall e\in E$ with capacity $(f_c - f_e) s.t. \lnot (f_e = f_c)$ there $\exists e_o$  with capacity $f_e s.t \lnot (f_e=0)$.

Below we provide an example demonstrating this in non-generic terms using a specific graph instance.

```
   Original Graph                Residual Graph

       5/7                           5
   a-------->d                   a------->d     We left out
   |   3/5.--^                  ^ <-------|^    middle edge.
5/5|.-----|  | 2/5              |    2    ||    What will it
   VV        |                5 |       3 ||2   be?
   b-------->c                  |    2    V  
       8/10                      b------->c
                                  <-------
                                      8
```

Residual networks are going to be useful because of the following property. Given a graph $G$ and a flow $f$ along with a residual graph $G_f$ with flow $g$. The flow $f+g$ is valid flow on graph $G$. Furthermore, if we want to calculate flow $f+g$ for a particular edge $e\in E$ we can do the following $f_e+g_e+g_e'$ where $g_e'$ is opposite flow of $g_e$.

### Maxflow-Mincut Theorem

We use _maxflow-mincut_ for finding bounds on _max flow_ and as bonus verify that the flow is optimal. To understand the theorem we first need to define what _cuts_ are. A cut $C$ is a set of vertices $v \in V s.t. C$ contains all $sources$ of $G$ and no $sinks$. The size of a cut is defined by $\lvert C \rvert= \sum_{out}f_c$. We demonstrate the definition of a cut on an example. In the example nodes labeled _t_ indicate $sinks$.


```
t     t     t
^     ^     ^                    Our cut is {a,b,c,d} and the       
|     |     |                    cut size is 8. In our example
|1    |1    |1                   the node d is the source.
|  1  |  1  |      5
a-----b-----c----------->t
|  1     1  |
.-----d-----.
```

We give the following lemma which we can use to set an upper bound on our flow. 

>Let $G$ be some graph, for any flow $f$ and any cut $C$ the following holds: $\lvert f \rvert \leq \lvert C \rvert$. 

From this we can derive the _maxflow-mincut_ theorem. The theorem simply implies that if we want the tightest possible bound on the flow we have to find the minimum cut. Formally this can summarized as $max \lvert f \rvert_{flows} = min \lvert C \rvert_{cuts}$.

Several interesting observations can be made. First around the the caveat we haven't considered, what happens when the max flow is 0? In this case there is no path from source to sink. If $C$ is set of vertices reachable from $source$ there is no edge out of $C$ so $\lvert C \rvert=0$. The other observation is that when we find some max flow $f$ in $G$ the max flow of $G_f$ will be 0. This is simply because there will be a cut of size 0 in $G_f$. This will be demonstrable in the next section when we do a walkthrough of the Ford-Fulkerson Algorithm.

### Ford-Fulkerson Algorithm

The underlying idea behind the Ford-Fulkerson is rather straightforward. We start with 0 flow and repeatedly add flow until we can add no more. We will be operating on the residual network $G_f$ and checking with every step the existence of a $source$ to $sink$ path. The step-wise execution of the algorithm is demonstrated below:

###### Step 1

We start with a network that has 0 flow in it. The source in this graph is assumed to be $a$ and the sink $c$. The residual network looks exactly like the original network with capacities $f_c$ on _forward edges_ equal to their original values and _backwards edges_ set to 0.

```
      Graph                     Residual Graph

       0/7                           7
   a-------->d                   a------->d     We left out
   |   0/3.--^                  ^|<-------|^    middle edge.
0/5|.-----|  | 0/5              ||   0    ||    What will it
   VV        |                 0||5      5||0   be?
   b-------->c                  |V   10   V  
       0/10                      b------->c
                                  <-------
                                      0
```

###### Step 2

With the next step we find a viable $path$ to the $sink$. We currently are not picky on the method for finding this $path$ so we will assume a standard _depth first search_. We will discuss the issues with this choice a bit later. As can be seen in the graph we take left edge out of $a$ into $b$ and then to $c$. Along this path we saturate the link with _maximum_ capacity that we can along that path, in more formal terms $min_{e\in path} C_e$. In the residual network we now add to the capacity along the _backwards_ edge on that path and _subtract_ it from the _forward_ edges.

```
      Graph                     Residual Graph

       0/7                           7
   a-------->d                   a------->d     We left out
   |   0/3.--^                  ^|<-------|^    middle edge.
5/5|.-----|  | 0/5              ||   0    ||    What will it
   VV        |                 5||0      5||0   be?
   b-------->c                  |V   5    V  
       5/10                      b------->c
                                  <-------
                                     5
```

###### Step 3

We are now not able to add more flow along the leftward $path$ so we have to go along the right. We again need to run _depth first search_ to find this $path$. We will assume that the search yields the $path$ that stays rightward. We repeat procedure as before to re-construct the residual network.

```
      Graph                     Residual Graph

       5/7                           2
   a-------->d                   a------->d     We left out
   |   0/3.--^                  ^|<-------|^    middle edge.
5/5|.-----|  | 5/5              ||   5    ||    What will it
   VV        |                 5||0      0||5   be?
   b-------->c                  |V   5    V  
       5/10                      b------->c
                                  <-------
                                     5
```

###### Step 4

By this step you can see that the cut $C$ which contains ${a,d,b}$ is _3_ this is still not the minimum. We still have capacity available on the _forward edge_ that we haven't used up. We will now use it. Notably we only have a capacity of _2_ left to use from $a$ to $b$. Once added in our residual network there will no longer be a viable path from $a$ to $c$. This means the cut $C$ containing only ${a}$ has the minimum cut size in our residual network, 0 as well as the flow. This also means we have found the best possible _flow_ in the original network.

```
      Graph                     Residual Graph

       7/7                           0
   a-------->d                   a------->d     We left out
   |   2/3.--^                  ^|<-------|^    middle edge.
5/5|.-----|  | 5/5              ||   7    ||    What will it
   VV        |                 5||0      0||5   be?
   b-------->c                  |V   3    V  
       7/10                      b------->c
                                  <-------
                                     7
```

We don't provide pseudo code as it can be found in plenty places online but we will discuss implementation details as it pertains to the way you do _graph traversal_.

### Edmonds-Karp Algorithm

The _Ford-Fulkerson_ algorithm specifically assumes the use of _depth first search_, but there is a less than obvious problem with this. To see it we first need to talk about the runtime complexity of this method. When doing complexity analysis we will assume _integrality_ constraint on capacity which also means we assume that flow itself will be an integer and not a float. Computing the residual network $G_f$ and find a $path$ from $source-sink$ is bounded by $\mathscr O(E)$ time. This should be obvious from the fact that in the worst case you will need to traverse the entire graph to find a path from $source-sink$ and that constructing a residual network simply involves adjusting _backwards edges_ along the $path$ you end up selecting. With every run we have a guarantee that flow will improve by at least _1_, this also is obvious since an improvement of _0_ would suggest there is no $source-sink$ path and we have our max flow. This thought can be extend by considering what if $\lvert f \rvert$ is large and we are at each step only improving by _1_. This is problematic.

To see this on an actual example, consider the following.

```
       0/100                                a is source and c is sink.
     a-------->d                            Consider what occurs if we are
     |   0/1.--|                            constantly picking the middle
0/100|.-----|  | 0/100                      edge in the residual network?
     VV        V           
     b-------->c            
       0/100                  
```

As is pointed out, the problem is that middle edge. Every iteration we would simply be picking that edge which would just flip the capacities on the _forward_ and _backward_ edges in the residual network. We would be doing this _200_ times until one of the alternative paths are finally saturated. On one hand, the problem is clear, we are picking the wrong $path$ each iteration, the better approach would be to take the leftward $path$ and then the rightward $path$. If we were to take the latter approach we would be done in _2_ steps. On the other hand the general rule of thumb around which _type_ of choices for paths are better and why they would be better is far less intuitive.

The key thing to observe is that the correct paths to take are _shorter_. This implies that we need to do a _breadth first search_ which, given equal edge weights, will naturally find the shortest possible $path$ from $source - sink$ first. Meaning we can easily make a modified version of _bfs_ that stores discovered paths data as it's doing the traversal and returns the first $path$ it manages to find from $source-sink$. An implementation caveat is that if the language doesn't support _tail-recursion_ optimization you might want to consider using a iterative implementation of _traversals_. This applies whether you implement _Edmonds-Karp_ or _Ford-Fulkerson_ variant.

To see why selecting the shortest path works much better consider what happens when you augment a path in one iteration of the algorithm. At least one edge when augmenting will become saturated. With _Ford-Fulkerson_ we are repeatedly saturating the same edge but here we guarantee to avoid that due to the following lemma:

>As _Edmonds-Karp_ executes, for some $v\in V$ the distance $d_{G_f}(s,v)$, where $s$ is source and $v$ is some node in the residual network along the path we are augmenting, increases. Similarly $d_{G_f}(v,t)$ and $d_{G_f}(s,t)$ where $t$ is sink can also only increase.

The path with the saturated edge on the path to $v$ is now undeniably going to be longer meaning on the next iteration _Edmonds-Karp_ algorithm picks _another_ shorter path. This extends to the _Re-use Limit Lemma_ which states:

> If an edge e is saturated, it will not be used in an augmenting path until $d_{G_f}(s,t)$ increases.

With this modification we can look back to the run time complexity of our algorithm. The distance $d_{G_f}(s,t)$ can only increase $\lvert V \rvert$ times and each time we are saturating at maximum $\lvert E \rvert$ edges. Therefore we can only consider $\mathscr O(\lvert V \rvert \lvert E \rvert)$
augmenting paths which takes $\mathscr O(\lvert E \rvert)$ to find using _traversal_. Therefore the run time will be $\mathscr O(\lvert V \rvert \lvert E \rvert^2)$. As you might have noticed, we removed the dependence on maximum flow.

## Bipartite Matching

Maximum flow discovery as it turns out has other useful and surprising applications. Starting with a what is known as _bipartite matching_ which can be used to solve assignment, scheduling, partitioning and many other types of problems. All of these solutions are based on being able to find maximal flow within a network. An example of a bipartite matching problem might for example be matching airline crew to flight. We could have some constraints on which crew member can be on which flight. These constraints might be because certain flights require crew members to speak certain languages to qualify as an attendant on that flight. More generally though we might have two disjointed sub sets of node $U$ and $V$ in graph $G$ that have edges between nodes in one set to the other to indicate that those particular nodes are compatible.


```
Crew                     Flight                          What's the best matching
                                                         amount we could have
James----*--------------->101                            in the following scenario?
Jane-----|-----*          102
John-----|-----|--------->103
Bob------*     *--------->104
Stacey ------------------>105

```

The problem of matching would be try and assign each crew member to each flight. More formally given a graph $G$, a matching on $G$ would be some collection of edges of $G$ such that no two edges share an endpoint. Typically we are solving so that the size of the collection will be maximized where a possible upper bound on the matching would be successfully matching all vertices in $U$ to all in $V$.

### Converting to Flow Problem

As noted prior we can use max flow to solve for bipartite matching. To do so we first need to construct a network $G'$ from bipartite graph $G$. This easy enough to do and is demonstrated below.

```
             Crew                     Flight
                                                           x is the source
     *-------James----*--------------->101-----*           and y is the sink.
x    *-------Jane-----|-----*          102-----*    y      We assume edge capacities
*----*-------John-----|-----|--------->103-----*----*      to be one.
     *-------Bob------*     *--------->104-----*
     *-------Stacey ------------------>105-----*

```

There is a one-to-one correspondence between a particular matching in $G$ and some flow in $G'$. With this knowledge all we need to do to solve for bipartite matching is construct $G'$ and compute max flow on $G'$ for which we know there is a corresponding matching that we can return.

### Konig's Theorem

We have discussed the interesting use of flow for finding optimal matching but presumably this would also imply that there has to be some form of connection between cuts and matching as well. To see what the relationship is, let's take some cut $C$ that encompasses some nodes in $U$ and $V$ along with the source. Next, let's take the intersection of the set of vertices in $C$ with all vertices in $U$ and repeat that but with all vertices in $V$. We will respectively call these resulting sets $X$ and $Y$, so formally, $X = C\cap U$ and $Y = C\cap V$. You can visualize this on the following example:

```
             Crew                     Flight
                                                           x is the source
     *-------James----*--------------->101-----*           and y is the sink.
x    *-------Jane-----|-----*          102-----*    y      We assume edge capacities
*----*-------John-----|-----|--------->103-----*----*      to be one.
     *-------Bob------*     *--------->104-----*
     *-------Stacey ------------------>105-----*


C contains x, John, Jane 103, 104
X contains John, Jane
Y contains 103, 104

```

This size of the cut $C$ in our above example will be the number of edges leaving $103$ and $104$ as well as the number of edges that are going from $x$ to $James$, $Bob$, and $Stacey$ _(circumventing or breaking the cut)_. The size of the cut will be 5 or more generally $\lvert U\setminus X\rvert+\lvert Y\rvert$. This is specific to case where we don't have edges going from $X$ to $Y$. The observation to make though is that all edges of $G$ will either connect to $Y$ or $U\setminus X$. This combined set serves as a bound on the best possible matching size. This can be extended to say that if find a maximal matching of some size $k$ then there will be a set $S$ of $k$ vertices so all edges of G will be adjacent to S, or more simply maximal matching is equivalent to finding the minimum vertex cover of the graph. This is Konig's Theorem.

### Conclusion

Thus endth part one of my notes on network flow and linear programming. Stay tuned for a later article that will review my notes on linear programming, a subject we have yet to talk about. The second part will explore various theoretical concepts and tie that discussion back to what we have learned here. Specifically, we will provide an overview of how we can use linear programming to solve for the max flow and minimum cut. If you have questions or concerns feel free to send me a message and I will try and answer it.
