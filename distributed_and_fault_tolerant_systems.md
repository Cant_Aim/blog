---
title: A Beginners Guide to Distributed and Fault Tolerant Algorithms
taxonomy:
    category: blog
    tag: [pram, complexity, analysis, map reduce, distributed computation, algorithms]
---

The goal of this article is to review some of the fundamental concepts of Distributed and Fault Tolerant Systems. We will discuss the challenges that occur when working inside the distributed space and some of the generalizations we can apply to simplify the design and analysis of our algorithms. In regards to analysis we will review what our metrics of goodness are and how we can categorize different levels of goodness based on the asymptotic complexity of our algorithms. This article seeks to only discuss some preliminary subjects. We will review a simple model for computation, challenges we face, and definitions that are necessary for us to understand. The original draft of the article included the review of _distributed storage_. This included topics that pertain to consensus but I quickly realized that this would be _way_ too much for a single post. I will make a future post that explores distributed storage topics. 

_All the material here is based on my notes from a graduate course I took with Professor Alexander Shvartsman as well as papers I have read._

===


## What are Distributed Systems?

The most obvious place to start this conversation is defining what counts as a distributed system. As it turns out the formal definition is broad enough that it basically includes most of the systems we work with day-to-day. Everything from multi-core processors, graphical-processors, local area clusters, to the Internet. Despite architectural differences there is a commonality between all these systems. They involve individual entities that have their own semi-independent agenda and **local view** of **global state**. But these entities are free to communicate or share resources with their peers to try and accomplish some grander task. The ability to divide labor intuitively should give us the benefits of speed-up, something we will take about later. Notably the rise of commercial and business distributed systems marks a very radical shift in how we tackle difficult problems with our hardware. Look up early 2000s articles on Intel promising consumer 10Ghz processors by 2010, turns out laws of thermodynamics still apply to Intel.


## What are the Challenges of Distributed Algorithm Design and Analysis?

TL;DR most our problems spawn from asynchrony, limited local knowledge, and failures. But more interesting is understanding why these are such big issues in the first place and also why they are notoriously hard to solve for. We will explore all that here!

One of the main goals of this post is understanding some of the **fundamental** difficulties of trying to create distributed algorithms. Do not view the problems noted here as being holistically representative of _all_ the challenges one might face. There are actually many architecture and system specific problems that we do not talk about here. An example of some of these problems include worker to coordinator promotion in the face of coordinator failure or byzantine failure models (_nodes working in an adversarial fashion against the rest of the system_). The challenges we discuss here are more broadly common to the entire topic of distributed systems and thusly are considered fundamental.  

### One Size Doesn't Fit All

Unlike Sequential algorithms, that have nearly a century of theory behind it, many advancements in distributed algorithms are actually relatively new. Not that computer scientists haven't thought about parallelizing code execution in the past. Methods for instruction pipelining and other classical computer architectural methods such as _Tomasulo's Algorithm_ have existed since the 70s. But these primitive methods are applicable to only a single processor and thusly doesn't allow for true asynchronous execution across multiple nodes. More importantly though for distributed algorithms we lack the same elegance that has been established for the sequential counter-part. This is best summarized by this (taken straight from my notes):

>_The phenomenon of general purpose computation in the sequential domain is both specific and powerful. We take the view here that it can be characterized by the simultaneous satisfaction of three requirements. First there exists at least one high-level language U that humans find satisfactory for expressing arbitrary complex algorithms. Second there exists a machine architecture M, in this case the von Neumann model, that can be implemented efficiently in existing technologies. Finally there exists a compilation algorithm A that can transform arbitrary programs in U to run on M efficiently._

Unfortunately this simplicity is absent in the distributed domain. There are multiple different ways in which we can interface with distributed systems. There are numerous feasible architectures that operate differently and that also implicitly means that different compilation methods exist for these architectures. To demonstrate this we can briefly explore some of the different choices and assumptions that are available even for our "ideal" model. Before doing that we will formally define a distributed system and it's parts:

The **topology** of the system can represented as an undirected graph where each node is a processor/worker and each edge is some communication channel like a bus between two nodes. The **network** is collection of all edges between nodes. Each node is assumed to be a regular machine able to execute local programs in a Turing-complete language. For each processor we define the current **state** it's in. The vector of all current active states is called the **configuration**. An **event** is something that changes the **configuration**. Thusly execution can be summarized in the following way.


Execution can be represented in the following way:

\[
C_0 \to A_1 \to C_1 \to A_2 \to C_2 \to A_3 \to ... \to A_N \to C_N
\]

Where every \(C\) represents a configuration and \(A\) represents an event. Configuration \(C\) is represented as the current configuration which is defined:

\[
C = {q_0, q_1, q_2, q_3... q_x}
\]

Where \(x\) is the number of processors in the system.


A successful system has two conditions that it needs to meet. The **safety** and **liveness** Conditions which in layman's terms can be though of as nothing bad happening for a finite prefix of the execution chain and something good must eventually happen, respectively. We will now continue our discussion regarding some different architectural choices or assumptions that are available to us:

* _**Asynchronous vs. Synchronous**_ - Whether our system allows for asynchrony is the first important choice to make. In an **asynchronous** system there is no explicit upper bound on how much time it will take for a message to reach a node. Thusly, the nodes don't proceed in lock-step. Some nodes maybe ahead of others in their respective work and might be finished executing before other nodes in the system. An execution is called **admissible** if each processor has an infinite set of computation events and every message sent is eventually delivered. The program terminates by having an event that continuously keeps a node in the same state, this state _should_ be a termination state. The message complexity of this system would be maximum of all admissible executions. **Synchronous** systems execute in lock-step. We have _rounds_ of execution where each round processors send messages to immediate neighbors and all processors do some amount of work based on what they just received, and then they carry on to the next round. The same requirement for admissibility applies to this round based structure and our definition of termination is also the same. Time complexity of our algorithm is easier to measure because we just need to count the number of steps we took.

* _**EREW vs. CREW vs. CRCW**_ - With shared memory systems we can have several rules as to how we can access resources. Exclusivity implies that only one processor can access the resource at a time. During this time all other processors are locked out from being able to work with that particular resource. Concurrence is the removal of that restriction. There are two operations that a processor can do to a resource in shared memory space and that is read and write. We can set concurrence/exclusivity restrictions on each operation individually. Thusly we can have the following rules: **Exclusive Read and Exclusive Write**, **Concurrent Read and Concurrent Write** or **Concurrent Read and Concurrent Write**. Each of the access patterns gets progressively stronger. With _EREW_ being the weakest and _CRCW_ being strongest.

* _**Common vs. Arbitrary vs. Priority**_ - One more way in which we can break down our operation rules is the write policy. This only applies to CRCW based systems. **Common** means that if multiple processors want to write to a resources they have to write the same value. **Arbitrary** means we make a random selection on the processor that can write to a resource. And **Priority** as expected implies a ranking to our processors and the one with the highest ranking can write the resource.

We will come back to all these concepts when we discuss what we call the **PRAM model**. The point here is to realize that are many architectural choices and unlike sequential execution systems theses choices will play a role in how we design algorithms. For example, algorithms that work for _EREW_ will work for _CRCW_ but the converse is not necessarily true. Another useful thing to think about is whether systems that are parameterized in a certain way can actually simulate other systems. As it turns out, yes they can! With an asynchronous system we can actually simulate synchrony. With _EREW_ we can simulate _Priority CRCW_. All of this can only be done with some degree of additional time overhead.

### The Issue of Consensus

We will now talk about the second fundamental problem of distributed systems, and that is consensus. To understand why this is a difficult problem there is a famous thought experiment that is presented in every networking and distributed systems course, usually on the first day. Consider the following problem:

We have two general's leading two separate armies, Let's call the first general Jon Snow and the second General Khaleesi; collectively they are both trying to lead an assault against Cersei at King's landing. Both Jon Snow's and Khaleesi's armies are separated by hills between which lies King's landing.

```
Jon Snow's Band of Wildings     King's landing        Khaleesi's Army of Unsullied

        |>             /\/\/\    |-------|    /\/\/\            |>
        |             /      \   |       |   /      \           |
  /\ /\ /\ /\ /\     /        \  |       |  /        \    /\ /\ /\ /\ /\
--------------------/          \-----------/          \-------------------
```

To successfully attack King's landing both Khaleesi and Jon Snow are going to have to coordinate the attack. If one party attacks before or after the other than the attack will fail. Khaleesi and Jon Snow are free to send messengers across the valley to communicate to the other side when they plan to attack and the other party can send a messenger back to confirm that they agree to attack at that time. The caveat is messengers that are sent across the valley are at risk of getting captured. The question is whether there exists a solution to the problem such that Khaleesi and Jon Snow can coordinate a successful attack. In short, can the two parties achieve consensus by communicating agreement?

A more generalized form of this problem exists called the _Byzantine Generals' Problem_ proposed by Leslie Lamport that applies to more than two generals. In that instance of the problem each general casts a vote, the tricky part being that some generals cast a vote purposely for a strategy that is considered suboptimal.

As it turns out there is no solution to this problem that guarantees 100% consensus amongst parties assuming we can't send infinite number of messages back and forth. To see this assume Jon Snow sends a message to Khaleesi to issue an attack at time _**x**_. Khaleesi then needs to send a message back confirming that she received the time when to attack. Otherwise Jon Snow doesn't have certainty that attacking is safe. Khaleesi then would need confirmation that the confirmation was received. The confirmations have the same probability of loss as the original message. There exists two proofs by contradiction (for the deterministic and non-deterministic case) that show it's impossible to achieve total agreement. We can however mitigate the risk!

All consensus and networking protocols, either paxos or tcp, are based not on the idea of not trying to eliminate uncertainty but reducing it, in fact by sending messages back and forth we easily test our channel for loss and also calculate the probability of success, although it can never be 100%.

Unfortunately, we don't talk much about consensus here but we will come back to the topic when we talk about maintaining consistency in a later post. This topic has very important implication in distributed storage and is a center piece to deciding the performance of our system and how it operates.

### Data Dependence

The most important inhibitor that prevents us from parallelizing many important and interesting problems is data dependency. The idea that it's impossible to break a problem down to individual _atomic_ components because all components are highly coupled in some way. Data dependency goes back to **instruction level parallelism** (ILP) which we are able to easily deal with by having occasional slow down of one instruction while waiting for another to evaluate, in processor design these dependencies are known as _hazards_. If you are unfamiliar with ILP I highly suggest looking at this [wikipedia article](https://en.wikipedia.org/wiki/Data_dependency). What we are going to focus on here will primarily be **task level parallelism**, or breaking down data sets into workable chunks that can fed to worker nodes. Some of the problems that intuitively fall are things like summation, which is very often demonstrated in the _MapReduce_ programming model. There are many of these sort of (useful) **embarrassing parallel** problems, _I.E problems where there is no data dependencies and it's very easy to chunk up the problem and get maximum speed up._ Some examples of these problem are:

* Summation.
* Distributed relational database queries (Without ACID transactions).
* Serving static files to multiple users. (Difficult when files are not static).
* Animation Rendering/Baking. (Frames can be render individually in parallel).
* Proof-of-Work Systems in crypto-currency.
* Brute-forcing Cryptography.
* Large scale facial recognition systems.
* Discrete Fourier transform where each harmonic is independently calculated.
* Gradient Descent Optimization (I.E Neural Nets).

There are also a lot of very important real-world problems that fall outside of this domain. Combinatorial optimization, this includes linear, integer, and mix-integer programming. Boolean Satisfiability Problem is another one which is difficult to parallelize which has implication on problems that belong to the _NP-Complete_ class of problems. Other problems like sorting can also be tricky to do efficiently. For example if we were to take the commonly used quick-sort, sequential equivalent, and try and parallelize it the pivot selection would still be largely sequential. Another point to be wary of is that quick-sort is actually not easy to implement on all distributed memory systems.

This leads us into our primary discussion of what we call Parallel Random Access Machines, otherwise known as PRAM, and how we can use that as convenient model for developing algorithms and analyzing those algorithms for efficiency. We will formally define what PRAM is and also give an explanation of how we measure efficiency in the context of parallel computing.

## How to Analyze Distributed Systems

The key reasons we use distributed systems is to achieve speed-up and robustness. We haven't yet discussed how to qualify systems as robust or efficient. In the following section we will specifically focus on qualifying different levels of speed up.

When deciding whether our distributed algorithm is efficient we want to compare it to the _best_ sequential algorithm. We will designate the time it takes the sequential algorithm to run as being \(T_1(N)\) where \(N\) is the input. The ideal speed up given \(P\) processors would be \(T_1(N)/P\). Notably we are interested in comparing to the _asymptotic lower bound_ of the best sequential algorithm rather than the upper bound.

If a \(P\) processor algorithm runs in time \(T_P(N)\) than we define speedup \(S\) as \(T_P(N)/T_1(N)\) which will be \(\theta(P)\) (_Theta implies a tight-bound_) when it's _linear_ or _optimal_. Most of the time our distributed algorithms will require overhead but using a simple technique, which we will talk about later, we will be able to achieve optimality in certain cases.

We now introduce an important concept, **work complexity** otherwise known as **cost**. Work can be used for estimating the total number of instructions executed across all processors during run-time or total resource allocation. Assuming we have synchrony the work \(W\) of our parallel algorithm can be defined formally as \(P T_P(N)\). When measuring work we are also taking into account idling processors waiting for their next instruction. The notion of work is also applicable to sequential algorithms, Where \(P\) would be set to 1 and thusly work is equivalent of \(T_1(N)\).

* We define **Optimal work** complexity as \(W = P T_P(N) = \theta(T_1(N))\). This can only be achieved with linear speed up. As mentioned previously there is typically overhead associated with most distributed algorithms thusly we have a few more "levels" to consider.

* We define **Polylogarithmically efficient work** complexity as \(W = P T_P(N) = \theta(T_1(N) log^{O(1)} (N)\). To achieve polylogarithmic efficiency by having the following non-linear speed up: \(S = T_1(N)/T_P(N)= \theta(P/log^c N)\).

* We define **Polynomially efficient work** complexity as \(W = P T_P(N) = \theta(T_1(N) N^{\epsilon})\) where \(\epsilon\) needs to be a constant \(0 \lesssim \epsilon \lesssim 1\). To achieve polynomial efficiency we need the following speed-up: \(S = T_1(N)/T_P(N) = Θ(P/N^{\epsilon})\). The reason we bound \(epsilon\) to be between 0 and 1 is that if we allow it to be greater than one we can essentially cheat. A distributed algorithm where \(P=N\) can simply run the best sequential algorithm on each processor (using the entire input) and it will be considered polynomially efficient by the formal definition.

Keep in mind that like classical complexity analysis this all only applies when our \(P\) and \(N\) values are none trivial, meaning they aren't constant values. In fact we are interested in being able to scale \(P\) with the size of \(N\). We also assume that any constants that maybe hidden away in the Big-O notation are small. Remember that the question of complexity is a question about how well does the solution scale, meaning how do our bounds tend as P and N converge to the limit at infinite.

Work complexity sets a stronger and stricter requirement than time complexity. In some cases where we can achieve best speed up using an algorithm with a polynomial number of algorithms relative to the input size we still may consider the algorithm work inefficient because it requires substantially more work than the sequential counter part. Furthermore work complexity is useful because we can use it to qualify same efficiency requirements when talking about **robustness**, an algorithm's ability to handle faultiness. We typically make an algorithm more robust by having more processors or sending more messages, Either of these increase the amount of work we need to do.

* **Optimal Robustness** is when \(W \leq c T(N)\) where \(c\) is a fixed constant.
* **Polylogarthmic robustness** is when \(W \leq c' T(N) log^c N\) where \(c\) and \(c'\) is a fixed constant.
* **Polynomial robustness** is when \(W \leq c T(N) N^{\epsilon}\) where \(c\) is a fixed contant and \(epsilon)\ is between 0 and 1.

In the case of robustness we measure work for a specific _failure model_ thusly the work may be function of not just \(P\) and \(N\) but also the _failure model_ that our algorithm should be able to handle. We won't go into the subject further here but it's worth reading about.

## The PRAM Model

We will now talk about the Ideal PRAM model. This is a convenient theoretical model because it helps mitigate one of the core problems of distributed computation that we talked about earlier, One Size Doesn't Fit All. PRAM gets us as close as possible to a universal model for distributed computation. In fact there are many distributed algorithms that have been written for and analyzed on PRAM. We like ideal PRAM also for the reason that it gives us the best upper bound for speed up or lower bound for work for the algorithms we built. PRAM is defined as synchronous system where each node or processor has shared memory access. These processors can concurrently access the shared location. There are \(Q\) "_memory cells_" where \(Q \lesssim N\) The first \(N\) cells hold the input the rest are zeroed initially. Each processor also has a private memory space that is typically of a small constant size.

### Tree Traversal Algorithm

We will review some Parallel Tree Traversal algorithms since they can be used by distributed systems to solve certain basic issues regarding coordination of nodes when solving problems. What we discuss here will also give some basis for where we get our complexity requirements for our efficiency categories. Everything we discuss here will be in the context of the PRAM model we just previously defined. That being said we will store our tree data structure in the form of the array, to keep in-line with the memory cell idea we presented.

```
        A         
      /  \            Translates         A B C D E F G
    B     C     -----------------------> 1 2 3 4 5 6 7
  / \    / \
D   E   F   G

To get the index of the left child of a given node we take it's index i: 2*i
To get the index of the right child of a given node we take it's index i: 2*i+1

This easily generalized to a q-ary tree by simply changing the multiplicative
factor to q and and being able to add upward to q on the constant term.
```

Traversing a single path of the tree will take \(log^q (N)\) where \(N\) is number of nodes we have in the tree. If we have \(P\) processors the work will be \(P log^q (N)\) Where each processor will either be trying to reach the root from the leaf or the leaf from the root, depending if try and do bottom up or top down traversal of the tree. This is already useful because it means we can do an elementary thing like depth first tree traversal of a binary tree in \(log(N)\) time instead of \(N\). With \(P=N\). Meaning we can do this with poly-logarithmically work efficiency. While this has practical applications for some basic problems, distributed system can use this for several crucial internal tasks that help dealing with failures and maintaining synchrony during execution.

* **Processor Enumeration** and **Work Evaluation** is the first of these tasks. It involves discovering which processor is currently active or working. **Work Evaluation** is the second of these tasks, which is actually implemented same as the previous task and enables us to figure out which processor is finished doing it's work.

```
Each of our processors starts at the leaf node and sets the value of 1 if it's
able if not then value will remain 0 and processor is assumed to be down.

          Processor Counting                 Work Evaluation

                  2                                  2
                /  \                               /  \
              0     2                            2     0
            / \    / \                         / \    / \
          0   0   1   1                       1  1   0   0

Upon writing in the leaf it will traverse to the parent node and add 1 to it.
When it's finished doing that it repeats that step at the parent of the node
it's currently on. Because addition is an idempotent operation we can
safely do this without having processors interfere with one another. We assume
that potential race condition issues are handled. We can reuse this
implementation for work evaluation.
```

* **Load balancing** is other these tasks and involves distributing processors to for processing our input. For load balancing we start with the results of the prior two algorithms meaning we have the summation trees on-hand from processor counting and work evaluation. We will use the information gather in those to properly load balance. We know we have two processors to use and we also know which problems need solving. Better yet,  work evaluation tree gives us a direction in how to get processors to the leaves where work needs to be done. The two available processors will traverse downward, allocating both processors leftward at the first level and then one left and one right on the second level. We don't go into the exact algorithm to assure that each processor proceeds in the right direction when a split is required but it is possible using the private memory space each processor has as well it's _PID_.

### Broadcast Algorithm

Using tree structure we could also construct a broadcast algorithm that can be used by a processor to tell all other processors about a value it's currently holding. We assume a simple binary tree with \(P\) nodes that is stored in an array \(B\) which is \([1...P]\). The processor that wishes to broadcast it's value to everyone else will first need to write the value it needs to share to the root. After this the processors at the other \(log P\) levels will read the values from their parent. The algorithm thusly takes \(\theta(log P)\) time and \(\theta(P log P)\) time.

```
Broadcast Algorithm

       P1         
      /  \            Translates          P1 P2 P3 P4 P5 P6 P7
    P2    P3     -----------------------> 1  2  3  4  5  6  7
   / \    / \
 P4  P5  P6 P7

The tree presented here is typically a minimum spanning tree that can be projected
over the the topology of our system. We use MSTs primarily for the reason that it
helps minimize channel cost required by our system. A one to many broadcast
requires 2^P channels in our system which doesn't scale.

First processors P1 initializes the value it wants to read at B[1].
Then processors P2 and P3 read the value from B[1] into B[2] and B[3] finally
processors P4 and P5 read the value from B[2] and processors P6 and P7 read the
value from B[3].  
```

The _key_ thing to see here is that algorithm requires concurrent reads. This is simple to change so that instead of having the children read the parent processors writes to the children. Thusly this works on **CRCW** and **CREW** systems as well as **EREW**.

### Summation Algorithm

The final of these basic tree traversal algorithms were going to look at is summation. Specifically we are going to examine how we can exploit **variable slackness** to to make our algorithm **work optimal**.

We present the same binary tree structure that we have used in the last few examples. This time though at the leaves instead of simply doing a read/write we will do a more advanced calculation. Because we are working on a summation problem at each leaf node we will take a sub-set of our input set and apply to it the best sequential summation algorithm which is linear time. We assume an input size of \(N\) and that \(P \leq N \). We have \(P\) processors that all start at the leaves each solving a chunk of the input at size \(N/P\) which we refer to as \(G\). There are \(log(2P)\) levels to the tree which we can simplify to \(log P\) (We are still operating in the confines of asymptotic notation, Meaning we can exclude constants and non-dominate terms.)

* As a first step each processor calculates the sum for it's set of values in synchrony. This takes \(G\) time or \(N/P\).

* Next the algorithm must have a _reducing_ number of processors proceed up the tree and sum up the value of it's children in synchrony. This process will take \(log(P)\) steps. At the leaves all P processors are involved in the calculation. At the next step it will be \(P/2\) processors, then \(P/4\) processors, then \(P/8\) processors. At each level a single processor will proceed to the parent from each of the pair of children. The processor that goes up will read values from the two children memory cells, sum those values, and save the resulting value in the current memory cell it's at. This process will take \(log(P)\) steps.

* The algorithm in total will run in \(\theta(N/P + log(P))\) time and the amount of work will be \(\theta(P(N/P + log(P)))\) or \(\theta(P + P log(P))\) which is the same as \(\theta(P log(P))\). \(P\) scales with \(N\) according to our specification so we can also rewrite our work complexity measure as \(\theta(N log(N))\).

We have thus far only managed to achieve _poly-logarithmically efficient work_. We can improve this to optimal by applying a really simple trick. We wish the following statement to be true. \(P log(N) = O(N)\). To achieve this we can play around with the variable \(P\). Specifically can make \(P\) scale sub-linearly to \(P\) by making it equal to \(O(N/log N)\) this is also known as **over-saturation**.

### The Do-All Problem

We will finally talk about the _**Do-All Problem**_, the cumulation of our previous conversation about tree traversal algorithms and variable slackness. This is fundamentally the problem which most distributed systems aim to solve. The problem can formally be defined as:

_p processors must cooperate together to perform n tasks in the presence of adversity._

Tasks are **similar**, **independent** and **idempotent**. Similar in the sense that they are running the same code on different input, independent in that they don't need to wait on another, and finally idempotent meaning running the same task multiple times doesn't _"ruin"_ the computation. An example of an idempotent operation is multiplying a number by zero. If you do it again the result will be still zero.

To see how this relates to our Tree Traversal methods remember our prior mention of Work Evaluation, Processor Enumeration, and Load Balance. We can use these concepts in the construction of our distributed system to enable it to solve the Do-All Problem. By repeatedly doing the following steps processor enumeration, load balancing, work at leaves, and work evaluation in rounds we can complete all tasks assigned to our distributed system. More so we can account for failures. At the beginning of the iteration we have a number of processors (potential an over-estimate) that we can distributed to the leaves. After finishing work at the leaves we evaluate which work we finished (potential an underestimate). Note, that even though we are over and under estimating this doesn't threat the validity of the algorithm.

Each iteration of our traversal steps we call a **block-step**. We can do work analysis for this algorithm the same way we did analysis for the individual traversals. There are several points worth noting before we do this though.

* Each block-step the number of processors is either the same as the last iteration or less. \(P_{i+1} \leq P_i\).

* Each block step processors are allocated to leaves where work has been unfinished. Each leaf will get no more than \(P_i/U_i\) processors. This implies that we balance our processors uniformly.

* Each block iteration there are less unfinished tasks than the iteration prior. \(U_{i+1} \leq U_i\). This guarantees that the algorithm finishes running at some point.

Next we need to think about how many block steps we can expect to do. To see how we may answer we need to consider the possible things that may occur at each individual block step.

* The first category of block steps is where we have less processors than unfinished tasks. For this category of block-steps each leaf gets no more than one processor according to our first point. There will be \(N\) of these block steps. In the equation below \(\tau\) is the number of iterations to finish all task.

\[
B_{case 1} \leq {\sum_{j=1}^\tau} (U_j - U_{j=1}) = U_1 - U_{\tau+1} = n - 0 = n
\]

* The next category of block steps is those where there are more processors than unfinished tasks but the number of unfinished tasks this iteration is less than \(U/(log log n)\) where \(U\) is the number of unfinished tasks on the last iteration. Because processors are failing at a slower rate we maintain upper bound of \(P\) and \(N\) for number of processors and unfinished tasks. There will be \(O(p log n/log log n)\) of these block steps.

* The final category of block steps is those where there are more processors than unfinished tasks but the number of unfinished tasks this iteration is equal to or more than \(U/(log log n)\) where \(U\) is the number of unfinished tasks on the last iteration. In this case the processors are failing at faster rate. The number of failed processors is at _least_ \(U_{j=1} {\frac{P_j}{U_j}} \geq \frac{U_j}{log n / log log n} \frac{P_j}{2U_j} \geq \frac{P_j}{2log n/log log n} \). The number of processors completing the \(l^{th}\) iteration are \(p(1-\frac{1}{2\frac{log n}{log log n}})^l\). Thusly at the There will be \(O(p (log n/log log  n))\) of these block steps.

\[
B_{case 2} \leq {\sum_{j=1}^\tau} p(1-\frac{1}{2\frac{log n}{log log n}})^l \leq p {\sum_{j=1}^\infty} (1-\frac{1}{2\frac{log n}{log log n}})^l = O(p (log n/log log n)
\]

In total we have \(O(n + p (log n/log log  n))\). Thusly to get total work we just need to multiple the work of a single block step with the total number of block-steps. \(O(n log n + p (log2n/log log n))\).

The algorithm we have presented here is specific to shared memory based systems and is based on global memory allocation. There are also local memory allocation and hashed memory allocation methods for shared memory systems. There are algorithms for solving the problem on message passing based systems using gossip and coordinator based coordination. Despite the fact that massive cluster PRAM systems are non-existent in practice it still holds as a very useful architecture independent model for developing and analyzing parallel algorithms. More so we are able to simulate PRAM in MapReduce based systems as is demonstrated in the following Stanford [paper](https://theory.stanford.edu/~sergei/papers/soda10-mrc.pdf) written by Howard Karloff, Siddharth Suri†, and Sergei Vassilvitskii‡.

