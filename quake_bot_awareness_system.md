---
title: Project Update: Quake 2 Bot Awareness and Input System 
taxonomy:
    category: blog
    tag: [quake, bot, artificial intelligence, rendering, architecture, reinforced learning, painters algorithm, game engine]
---

Quake 2 Bot is a learning exercise to try and develop a bot that can play duel matches on _q2dm1_ in Quake 2 at a decent level that is either comparable to veteran player _(stretch goal)_ or a bot that is believably adaptive _(more likely result)_. Quake 2 by default came with no bots and required community intervention to include them. The most well known of these are the _Gladiator_ bots created by _Johannes Marinus Paulus van Waveren_ aka _Mr.Elusive_. _Mr.Elusive_ was formally hired by Id and developed the Quake 3 bots as well. A 2001 white paper cleverly called ["The Quake 3 Arena Bots"](https://fabiensanglard.net/fd_proxy/quake3/The-Quake-III-Arena-Bot.pdf) was published explaining the details of how the quake 3 bots work. For this project we will be borrowing concepts from that paper and layer modern reinforced learning techniques and tools. 

===


## Introduction

Because Quake 2 is an incredibly old game we first had to do a bunch of work to expose a working interface to the game's internal state. This internal state data our **agent** would be able to use to construct a _current view_ of the **environment** and also feed back **actions** telling the game what to do next. Unlike the recent [Deepmind Quake 3 CTF project](https://arxiv.org/pdf/1807.01281.pdf) we will not be using pixel data from the frame buffer but rather just have the game feed back information directly on what it currently is rendering, or in other words what the player _(or bot)_ is currently seeing and hearing. Hopefully this makes the problem easier because we won't have to deal with the added layer of complexity of having the agent have to recognize what it's looking at. This also means that the data feed to the agent will be far more concise. The main objective of this project isn't to develop a bot that mimics human reaction time and aiming precision but rather a bot that can find the optimal strategy in a duel given robot like reaction time and aim. Historically, bots even with these advantages still lose to competent players simply because decision making process in a competitive duel is on infinite and continuous state space and genetic optimization algorithms as described in the white paper linked above yield bots that cannot out think a human player. 

Duels on _q2dm1_ in _Quake 2_ are highly coveted in the quake community because of how nuanced and complicated they can get. There are players who have been playing just this map in Quake 2 for the last 21 years. The map is notoriously difficult to learn and seemingly has massive skill gaps between even the top players indicating that randomness is a minimal factor. If you are in fact better than another player you will always win. Videos and demos of matches like [this](https://www.youtube.com/watch?v=yFen-s7Btqo) can be found on various sites. As a bonus you can also review community member's Myrmidon's guide on dueling [here](http://panjoo.tastyspleen.net/downloads/quake2-theory-guide-by-myrmidon.pdf) to gain an intuition of why Quake 2 Duels are interesting.

As mentioned before, this update will serve to explain the foundational work that had to be done to allow us to begin working on the reinforced learning component. One additional piece of information that maybe useful is that we will **not** be using the original Quake 2 source code since it's an absolute mess. Instead we will use a refactored and cleaned up version that is currently maintained called [yquake2](https://github.com/yquake2/yquake2). This code still is hard to parse but is far more manageable than the original. In the rest of this write-up we will review the underlying architecture of Quake 2, explain our modifications to it, demonstrate the IPC that occurs between Quake and our Python launcher, and conclude with a further explanation of how the bot awareness and input systems will be utilized. To follow the model offered by _Mr.Elusive_ in his design of the Quake 3 bots the architecture of our bots and what we have accomplished to date is layer 1 of the following diagram:

```            
                  *-----------------------------------------*
                  |                                         |
                  |                                         |
       Layers 1-n |                  ???                    |   In process of researching.
                  |                                         |
                  |                                         |
                  *-----------------------*-----------------*
          Layer 1 | area awareness system | basic actions   |   Currently implemented and necessary for higher layers.
                  *-----------------------*-----------------*
```

The objective in the next iteration of the project will be figuring out and implementing layers that will utilize our Layer 1 components.

## Understanding the Quake 2 Architecture

Quake 2 is game from another era and doesn't follow many of software standards and practices that a developer would be familiar with today like unit tests. Even when comparing to the next iteration of Quake, Quake 3: Arena, the difference in code quality is noticeable. A glance at the Quake 2 code and things will immediately feel like they were rushed and ductaped together. Despite that there are plenty of examples of creative problem solving in it. One such example is how Quake achieves polymorphism with C and Dynamic Linking which allows for interesting things like the ability to switch out the renderer during run time. Also, this is one of the last few games to do software rendering without the use of 3d accelerators like directX or openGL. Fabien Sanglard provides a very thorough overview of the game's entire architecture on his [blog](https://fabiensanglard.net/quake2/index.php). Here we will focus on the pieces of the architecture that matter to us.

At the time of this being written we are on the version of _yquake2_ at commit `d43a770` which includes a small fix by us to have the game correctly parse the `maps.lst` file which was merged into the main fork of _yquake2_. The project structure of this version of the game has been cleaned up and makes it easier to see the separation between various components when browsing the source.

### Project Structure and Rendering Pipeline

We quickly provide what the project structure is. You might wonder why server and client code is bundled together. This was an improvement made over Quake, the client can now act as either server or client or both. Fabien Sanglard, again, provides more detail on his blog. This is also visible in the game's behavior where the game will spin up a local server even when launching a single player game.

```
src\
  |-backends
  |-client\
         |-sound\
               |-openal.c
         |-refresh\
                 |-gl1
                 |-gl2
                 \-soft
         |-cl_view.c
         |-cl_input.c
         | ...
         \-...
  |-common
  |-game
  |-server
  \-win-wrapper
```

The part that pertains most to us is the client code, specifically the part that is responsible for invoking the initialization parameters for the rendering pipeline. This occurs in the `client` directory in the `cl_view.c` file. The call to method `void V_RenderView(float stereo_separation)` occurs for every frame. `V_RenderView` is not the renderer code itself. As noted prior the game allows you to dynamically switch renderers. The rendering code is found under the `client\refresh` folder. Each folder `gl1`, `gl2`, and `soft` corresponds to an `openGL1`, `openGL2`, and `software` renderers. Inside of these you will have `R_RenderView` which will run the actual rendering pipeline which is summarized below:

```
R_PushDlights - Mark polygon affected by dynamic light
R_SetupFrame
R_SetFrustum
R_SetupGL - Setup GL_MODELVIEW and GL_PROJECTION
R_MarkLeaves - Decompress the PVS and mark potentially Visible Polygons
R_DrawWorld - Render Map, cull entire clusters of polygons via BoundingBox Testing
R_DrawEntitiesOnList - Render entities
R_RenderDlights - Blend dynamic lights
R_DrawParticles - Draw particles
R_DrawAlphaSurfaces - Alpha blend translucent surfaces
R_Flash - Post effects (full screen red for damage, etc...)
```

There is a lot that each of these do but we will focus in on `R_DrawEntitiesOnList` which is responsible for rendering non-brush assets such as models. This method internally will loop over entities that the player currently _sees_: 

```C
// ...more code here...

for (i = 0; i < r_newrefdef.num_entities; i++)
{
	currententity = &r_newrefdef.entities[i];
	
	// ...more code here...
```

A key component of real time renderers is that they will avoid rendering more than they have too. Game's have data structures responsible for keeping track of what to render, a classic example of this are _BSP trees_ which are produced by the _painters algorithm_ and then used to figure out what needs to be rendered based on where in the tree the player currently is. While we won't be accessing _BSP trees_ directly we can see from the excerpt above where the engine is storing entities that it wants to render in the current frame. Our goal will be to figure out what these are and then extract them since they directly correspond to the things the player _(or in our case, the bot)_ sees.

Good! So now let's revisit `V_RenderView`. As it turns out, `V_RenderView` will actually construct the `r_newrefdef` object we were looking at above, it will delegate work to construct the entity list to another method called `CL_AddEntities`:


```C
/* build a refresh entity list and calc cl.sim*
this also calls CL_CalcViewValues which loads
v_forward, etc. */
CL_AddEntities();

// ...more code here...
```

After this call it will do some additional work to fill the `refdef_t` struct completely which is shown below:

```C
typedef struct {
    int	x, y, width, height;   /* in virtual screen coordinates */
    float fov_x, fov_y;
    float vieworg[3];
    float viewangles[3];
    float blend[4];            /* rgba 0-1 full screen blend */
    float time;                /* time is uesed to auto animate */
    int	rdflags;               /* RDF_UNDERWATER, etc */
    byte *areabits;            /* if not NULL, only areas with set bits will be drawn */
    lightstyle_t *lightstyles; /* [MAX_LIGHTSTYLES] */
    int	num_entities;
    entity_t *entities;
    int num_dlights;           // <= 32 (MAX_DLIGHTS)
    dlight_t *dlights;
    int	num_particles;
    particle_t *particles;
} refdef_t;
```

The cool thing is that a fully initialized instance of this struct has _almost_ everything that we need our bot to know, including its position and view orientation. What's missing then? The sound the bot hears, player state and a couple of additional metadata points we will extrapolate using the data we have.

### Producing and Spatializing Sound

This was actually not to hard to find. Quake uses `OpenAL` for generation of multichannel three-dimensional positional audio. In the `client\sound` folder we have the file `openal.c` that contains the code that makes calls to the `OpenAL` interface. Conveniently the code applies spatialization right next to where it invokes the method to play the sound. This all occurs inside the `void AL_PlayChannel(channel_t *ch)` method. The `channel_t` struct has all relevant information about the sound by the time it's played with the call to `qalSourcePlay(ch->srcnum)` occurs.

```C
typedef struct
{
    sfx_t *sfx;                 /* sfx number */
    int leftvol;                /* 0-255 volume */
    int rightvol;               /* 0-255 volume */
    int end;                    /* end time in global paintsamples */
    int pos;                    /* sample position in sfx */
    int looping;                /* where to loop, -1 = no looping */
    int entnum;                 /* to allow overriding a specific sound */
    int entchannel;
    vec3_t origin;              /* only use if fixed_origin is set */
    vec_t dist_mult;            /* distance multiplier (attenuation/clipK) */
    int master_vol;             /* 0-255 master volume */
    qboolean fixed_origin;      /* use origin instead of fetching entnum's origin */
    qboolean autosound;         /* from an entity->sound, cleared each frame */

#if USE_OPENAL
    int autoframe;
    float oal_vol;
    int srcnum;
#endif
} channel_t;
```

The key fields in the struct are the `origin` vector and `sfx` pointer from which we can get where the sound originated from and what that sound was.

### Ascertaining Player State

The `refdef_t` instance that we access inside of `V_RenderView` is stored inside of a `client_state_t` instance. This struct has a field `frame` inside it that is of type `frame_t`. Inside of `frame_t` we have a `playerstate` field of type `player_state_t`:

```C
typedef struct
{
    qboolean valid;                 /* cleared if delta parsing was invalid */
    int	serverframe;
    int	servertime;                 /* server time the message is valid for (in msec) */
    int	deltaframe;
    byte areabits[MAX_MAP_AREAS/8]; /* portalarea visibility bits */
    player_state_t playerstate;
    int num_entities;
    int parse_entities;             /* non-masked index into cl_parse_entities array */
} frame_t;
```

The `player_state_t` is defined as:

```C
typedef struct
{
    pmove_state_t pmove;        /* for prediction */
    vec3_t viewangles;          /* for fixed views */
    vec3_t viewoffset;          /* add to pmovestate->origin */
    vec3_t kick_angles;         /* add to view direction to get render angles */
	                            /* set by weapon kicks, pain effects, etc */
    vec3_t gunangles;
    vec3_t gunoffset;
    int gunindex;
    int gunframe;
    float blend[4];             /* rgba full screen effect */
    float fov;                  /* horizontal field of view */
    int rdflags;                /* refdef flags */
    short stats[MAX_STATS];     /* fast status bar updates */
} player_state_t;
```

the `stats` array holds information necessary to draw player information on the screen, things like health, armor, current weapon, etc. We can access what we want using the following macros:

```C
/* player_state->stats[] indexes */
#define STAT_HEALTH_ICON 0
#define STAT_HEALTH 1
#define STAT_AMMO_ICON 2
#define STAT_AMMO 3
#define STAT_ARMOR_ICON 4
#define STAT_ARMOR 5
#define STAT_SELECTED_ICON 6
#define STAT_PICKUP_ICON 7
#define STAT_PICKUP_STRING 8
#define STAT_TIMER_ICON 9
#define STAT_TIMER 10
#define STAT_HELPICON 11
#define STAT_SELECTED_ITEM 12
#define STAT_LAYOUTS 13
#define STAT_FRAGS 14
#define STAT_FLASHES 15           /* cleared each frame, 1 = health, 2 = armor */
#define STAT_CHASE 16
#define STAT_SPECTATOR 17
```

Now that we have identified what we want to extract _(read)_ from the Quake 2 engine we can proceed to do it. Before moving on to our modifications to the engine we will also briefly discuss how we can feed input back to the game since it's crucial for a reinforced learning agent to be able to interact with its environment.

### Feeding Input to Quake

First layer of control that Quake provides us is a programmatic interface to the console window. We can feed console commands to the game using the following methods calls: 

* `void Cvar_SetValue(char *var_name, float value)`
* `cvar_t *Cvar_Set(char *var_name, char *value)`
* `void Cbuf_AddText(char *text)`

This will allows to control game settings. Everything from client networking options around which servers we connect to, to game rules when creating a server, etc. The second layer of control is less parametric but rather player actions in-game. Player actions that are typically triggered using key presses can easily be triggered using any of the `IN_*` methods defined inside of `cl_input.c`.

```C
// ...more code here...

void IN_KLookDown(void)
{
    KeyDown(&in_klook);
}

void IN_KLookUp(void)
{
    KeyUp(&in_klook);
}

// ...more code here...
```

In the next sections we will discuss how we modified the Quake 2 source as well as how we put a Python launcher around quake and established IPC between Quake and our launcher. We will provide a demonstration of our launcher and agent using QubeOS system as our test environment.

## Modifications to Quake 2

Now that we have discovered where information relevant to agent exist we need to extract it. For that we add a new file under the `client` folder called `cl_agent.c`. This will have custom methods that will parse necessary structures. We will make several of these methods: 

* `void GymCaptureCurrentPlayerViewStateCL(refdef_t refdef, player_state_t state)`
* `void GymCaptureCurrentPlayerSoundStateCL(channel_t *ch)`. 

The `GymCaptureCurrentPlayerViewStateCL` method is meant to grab the player view data and state data. To do so we make a call to it under the `V_RenderView` method in `cl_view.c` file. We make the call right at the end of the `V_RenderView` where the `refdef_t` and `player_state_t` structs are guaranteed to be fully initialized for the current frame.

```C
// ...more code here...

SCR_DrawCrosshair();

GymCaptureCurrentPlayerViewStateCL(cl.refdef, cl.frame.playerstate);
```

We also need to grab the sound data, this we can do in the `openal.c` file. Once we have spatialized the sound we can pass the `channel_t` struct to our method.

```C
// ...more code here...

/* Spatialize it */
AL_Spatialize(ch);

/* Capture details of sound that is going to be played */
GymCaptureCurrentPlayerSoundStateCL(ch);

// ...more code here...
```

Now we can examine the details of how we parse the data and extrapolate additional information from what we have. We defined a custom `message_t` struct that we will use for the purpose of storing the combined information that we want our agent to know. Below the commented values tell the approximate domain size of each field. So `time` has value of `600` because a match is expected to be 10 minutes and the measure for time is in seconds. We will have a single instance of `message_t` called `Message` that we will be re-using each frame and updating. Meaning if for example the number of `frags` between frame `f` and frame `f+1` doesn't change then `frags` in `Message` will stay the same as it was in the last frame.

What is interesting here is that the supposed domain for the values  provides us a rough estimate of the state space we will dealing with when trying to do policy optimization using Reinforced Learning. Assuming these exact variables will compose our linear combination of features, with domain constraints of 0 to _value shown in comment_, then the total state space size will be `(1000^3)(100^6)(600)(700)(200)(2)(10)` or `1.68e+30`. 

```C
typedef struct
{
  float playerPositionX; // 1000
  float playerPositionY; // 1000
  float playerPositionZ; // 1000
  float playerViewAngleX; // 100
  float playerViewAngleY; // 100
  float playerViewAngleZ; // 100
  int time; // 600
  int frags; // 10
  int enemyLooking; // 2
  float enemySoundDistance; // 700
  float enemyPositionX; // 100
  float enemyPositionY; // 100
  float enemyPositionZ; // 100
  float projectileDistance; // 200
} message_t;
```

When we start the first values of our `Message` buffer will be the following, we set infeasible values outside the expected domain for our values. This means we will know when a true observation of a feature is made:

```C
void GymInitializeMessage(){
  Message.playerPositionX = -99999.0;
  Message.playerPositionY = -99999.0;
  Message.playerPositionZ = -99999.0;
  Message.playerViewAngleX = -99999.0;
  Message.playerViewAngleY = -99999.0;
  Message.playerViewAngleZ = -99999.0;
  Message.time = -1;
  Message.frags = -1;
  Message.enemyLooking = -1;
  Message.enemyPositionX = -99999.0;
  Message.enemyPositionY = -99999.0;
  Message.enemyPositionZ = -99999.0;
  Message.projectileDistance = -99999.0;
}
```

### Details of Capture Current Player View State

The entry points are themselves fairly straight forward. Below is the code for the `GymCaptureCurrentPlayerViewStateCL` method:

```C
void GymCaptureCurrentPlayerViewStateCL(refdef_t refdef, player_state_t state)
{
  /* Start server */
  GymStartServer();

  char buf[10000];
  entity_t *entity;
  float prior = 99999.0;

  GymCapturePlayerStateCL(refdef, state);
  for (int i = 0; i < refdef.num_entities; i++){
    entity = &refdef.entities[i];
    GymCaptureEntityStateCL(refdef, entity, prior);
  }

  GymMessageToBuffer(Message, buf);
}
```

The steps of the method are the following:

* Start gym server if not yet started.
* Capture player state.
* Capture visible entity states.
* Pass state to the message buffer for server to forward to launcher Python program.

`GymCapturePlayerStateCL` will update the following fields:

```C
Message.playerPositionX = refdef.vieworg[0];
Message.playerPositionY = refdef.vieworg[1];
Message.playerPositionZ = refdef.vieworg[2];
Message.playerViewAngleYaw = refdef.viewangles[0];
Message.playerViewAnglePitch = refdef.viewangles[1];
Message.playerViewAngleRoll = refdef.viewangles[2];
Message.time = state.stats[STAT_TIMER];
Message.frags = state.stats[STAT_FRAGS];
```

This includes the `x`,`y`, and `z` position of the player on the map as well as the view angle of the player in degrees along `yaw`, `pitch`, and `roll`. Also, the number of `frags` and `time` into the match. We also parse each visible entity and obtain what we need. Below is the code for the `GymCaptureEntityStateCL`:

```C
void
GymCaptureEntityStateCL(refdef_t refdef, entity_t *entity, float prior){
  int front = GymCheckIfInFrontCL(refdef.viewangles,
					 refdef.vieworg,
					 entity->origin);

  int looking = GymCheckIfInFrontCL(entity->angles,
					   entity->origin,
					   refdef.vieworg);

  int visible = GymCheckIfIsVisibleCL(refdef.vieworg,
					   entity->origin);

  float distance = GymCheckDistanceTo(refdef.vieworg,
				      entity->origin);
  
  // ...code to stdout what we captured is here but we ignore it...
  
  const char *actual = &entity->model;
  const char *player = "dmspot";
  const char *rocket = "rocket";
  const char *grenade = "grenade";
  if(strstr(actual, player) != NULL && front && visible) {
    Message.enemyLooking = looking;
    Message.enemyPositionX = entity->origin[0];
    Message.enemyPositionY = entity->origin[1];
    Message.enemyPositionZ = entity->origin[2];
  } else if((strstr(actual, rocket) != NULL
	     || strstr(actual, grenade) != NULL)
	    && distance < Message.projectileDistance) {
    Message.projectileDistance = distance;
  }    
}
```

This class is more involved and we will review it more closely. First what we do is extrapolate some spatial details of this entity object in relation to the player. `front` is whether the object is in the general direction of the where the player is looking. `visible` is whether that object is visible to player by seeing whether it can draw a line from the player to the object without clipping brush or another asset. `distance` is the distance between the player and object. `looking` is whether the object, in the case of being an enemy, is looking at the player. In the current state we are not capturing all objects that a player sees but just the closest projectile object like a `rocket` or a `grenade` as well as the `enemy player`. We can extend this to capturing a fixed set of visible objects for example upwards of 100 objects. Each of the spatial information is gotten using basic linear algebra. For drawing a collision trace between the player and the object we use the included engine method `CM_BoxTrace` which is used in `ai` code and other locations as basic primitive for solving problems.

#### Calculating Front, Visible, Looking, and Distance Values

To calculate the front value we need to use our angles along the `yaw`, `pitch`, and `roll`. We can then produce a vector to which we can apply vector arithmetic. The `AngleVectors(vec3_t angles, vec3_t forward, vec3_t right, vec3_t up)` method does this automatically for us. We are only interested in the forward vector so we pass `NULL` to the `right` and `up` parameters. The method calculates this forward vector in the following way:

```C
float angle;
static float sr, sp, sy, cr, cp, cy;

angle = angles[YAW] * (M_PI * 2 / 360);
sy = (float)sin(angle);
cy = (float)cos(angle);
angle = angles[PITCH] * (M_PI * 2 / 360);
sp = (float)sin(angle);
cp = (float)cos(angle);
angle = angles[ROLL] * (M_PI * 2 / 360);
sr = (float)sin(angle);
cr = (float)cos(angle);

if(forward)
{
    forward[0] = cp * cy;
    forward[1] = cp * sy;
    forward[2] = -sp;
}
```

We then take the difference between the positional vectors of the player and entity to get a directional vector where the components are projected distances along `x`, `y`, and `z` axes. We normalize this vector to a unit vector and take the dot product of that vector with the forward vector that we got with `AngleVectors`. If the dot product is less than `.3` then we know that the entity is within the cone of vision of the player. 

```C
int
GymCheckIfInFrontCL(float view[3], float source[3], float dest[3]){
  //Given variables
  vec3_t source_vec;
  vec3_t dest_vec;
  vec3_t view_vec;

  source_vec[0] = source[0];
  source_vec[1] = source[1];
  source_vec[2] = source[2];

  dest_vec[0] = dest[0];
  dest_vec[1] = dest[1];
  dest_vec[2] = dest[2];

  view_vec[0] = view[0];
  view_vec[1] = view[1];
  view_vec[2] = view[2];

  //Calculated Variables
  vec3_t result_vec;
  vec3_t forward_vec;
  float dot;

  AngleVectors(view_vec, forward_vec, NULL, NULL);

  VectorSubtract(dest_vec, source_vec, result_vec);
  VectorNormalize(result_vec);
  dot = DotProduct(result_vec, forward_vec);

  if (dot > 0.3)
    {
      return 1;
    }

  return 0;
}
```

The same can be done in reverse to see whether the entity (given it's an enemy player) is looking back at the player. All we do is switch the `view` vector to that of the entity's and the source to the position of the entity and destination to the position of the player. As mentioned above the `visible` variable is discovered using an engine helper method. To get distance we do a simple euclidean distance calculation in 3 dimensional space.

```C
float
GymCheckDistanceTo(float source[3], float dest[3])
{
  vec3_t difference;
  difference[0] = dest[0] - source[0];
  difference[1] = dest[1] - source[1];
  difference[2] = dest[2] - source[2];
  return powf(powf(difference[0],2) + powf(difference[1],2) + powf(difference[2],2), .5);
}
```

### Details of Capture Current Player Sound State

```C
void GymCaptureCurrentPlayerSoundStateCL(channel_t *ch)
{
  char buf[10000];
  vec3_t source;
  vec3_t dest;
  GymStartServer();
  source[0] = Message.playerPositionX;
  source[1] = Message.playerPositionY;
  source[2] = Message.playerPositionZ;
  dest[0] = ch->origin[0];
  dest[1] = ch->origin[1];
  dest[2] = ch->origin[2];
  Message.lastSoundDistance = GymCheckDistanceTo(source, dest);
  GymMessageToBuffer(Message, buf);
}
```

Capturing sound data is fairly straight forward. Whenever a sound event occurs `GymCaptureCurrentPlayerSoundStateCL` will be called, it will calculate the distance of sound using spatialization information `channel_t` struct and will set the `lastSoundDistance` field appropriately. This could either be the player's own sound or the enemy's. As in the `GymCaptureCurrentPlayerViewStateCL` we pass off the `Message`, in its current state, to our _"server"_ to then pass to our python launcher program. In the next section we will show samples of full data captured each frame and also what is contained in the `Message` when it's passed off. We follow that up with an explanation of the IPC that occurs between Quake and Python and why it's needed.

### Sample of Full Data Capture in a Given Frame

![quake 2 awareness demonstration 1 gif](https://raw.githubusercontent.com/CanntAim/yquake2-bot/grabbing-game-state-sockets/gym/documentation/peek_quake_base_1.gif)

We first examine the full data capture for some given frame. We show a screenshot of the given frame and also provide the log entry for the data capture. We demonstrate the capture on a single player map `base1` or less formally the first map of the main campaign:

![quake 2 awareness demonstration 1](https://gitlab.com/Cant_Aim/blog/raw/master/quake_2_demo_awareness_system_singleplayer.png)

We show the log output for the frame seen above. As you can see under `Current View` the health of the player in the screenshot matches what we captured under the health field. Also the view angle matches what you see in the screenshot, there is no pitch or roll in the camera either. Under the `Entities for current frame` the first set of entities are labeled as `map/*.bsp`. These appear to correspond to perhaps the map brush although we are not certain on this detail. The ones we are interested are `*/tris.md2` entities that are being captured. All of these are entities that are within the immediate vicinity of the player. The ones that are in the cone of vision of the player have `Is in front of player:` labeled as `1`, otherwise the value is `0`. We also have the `Is visible to player:` field which defines whether there is no obstruction between the player and the entity. For an object to be considered seen both these values have to be `1`.


```
...
...Current View:
...
Position of player: ...x: -208.437500 ...y: -70.062500 ...z: -1.812500
View angle of player: ...pitch: 0.000000 ...yaw: 164.437866 ...roll: 0.000000
Player Static State: 
Health: 48
Armor: 0
Time: 0
Frags: 0
Current Armor Type: 0
Current Weapon: 0
...Entities for current frame:
...
drawing entity: maps/base1.bsp ...x: 0.000000 ...y: 0.000000 ...z: 0.000000
Is in front of player: 0
Is visible to player: 0
Entity looking at player: 0
drawing entity: maps/base1.bsp ...x: 0.000000 ...y: 0.000000 ...z: 0.000000
Is in front of player: 0
Is visible to player: 0
Entity looking at player: 0
drawing entity: maps/base1.bsp ...x: 0.000000 ...y: 0.000000 ...z: 0.000000
Is in front of player: 0
Is visible to player: 0
Entity looking at player: 0
drawing entity: models/weapons/v_blast/tris.md2 ...x: -208.500000 ...y: -70.125000 ...z: -1.875000
Is in front of player: 1
Is visible to player: 1
Entity looking at player: 0
drawing entity: models/deadbods/dude/tris.md2 ...x: -962.000000 ...y: 248.000000 ...z: -48.000000
Is in front of player: 1
Is visible to player: 0
Entity looking at player: 1
drawing entity: models/deadbods/dude/tris.md2 ...x: -894.000000 ...y: 258.000000 ...z: -48.000000
Is in front of player: 1
Is visible to player: 0
Entity looking at player: 1
drawing entity: models/deadbods/dude/tris.md2 ...x: -656.000000 ...y: 136.000000 ...z: -48.000000
Is in front of player: 1
Is visible to player: 0
Entity looking at player: 1
drawing entity: models/deadbods/dude/tris.md2 ...x: -632.000000 ...y: 192.000000 ...z: -48.000000
Is in front of player: 1
Is visible to player: 0
Entity looking at player: 1
drawing entity: models/deadbods/dude/tris.md2 ...x: 40.000000 ...y: 272.000000 ...z: -64.000000
Is in front of player: 0
Is visible to player: 0
Entity looking at player: 0
drawing entity: models/deadbods/dude/tris.md2 ...x: 432.000000 ...y: 248.000000 ...z: -64.000000
Is in front of player: 0
Is visible to player: 0
Entity looking at player: 0
drawing entity: models/deadbods/dude/tris.md2 ...x: -384.000000 ...y: 0.000000 ...z: -48.000000
Is in front of player: 1
Is visible to player: 1
Entity looking at player: 1
drawing entity: models/items/healing/medium/tris.md2 ...x: -536.000000 ...y: 424.000000 ...z: -32.875000
Is in front of player: 1
Is visible to player: 0
Entity looking at player: 1
drawing entity: models/items/armor/shard/tris.md2 ...x: -976.000000 ...y: 296.000000 ...z: -32.875000
Is in front of player: 1
Is visible to player: 0
Entity looking at player: 0
drawing entity: models/items/armor/shard/tris.md2 ...x: -928.000000 ...y: 264.000000 ...z: -32.875000
Is in front of player: 1
Is visible to player: 0
Entity looking at player: 0
drawing entity: models/items/healing/stimpack/tris.md2 ...x: -384.000000 ...y: 80.000000 ...z: -32.875000
Is in front of player: 1
Is visible to player: 1
Entity looking at player: 1
drawing entity: models/items/healing/stimpack/tris.md2 ...x: -336.000000 ...y: 80.000000 ...z: -32.875000
Is in front of player: 1
Is visible to player: 1
Entity looking at player: 1
drawing entity: models/items/armor/jacket/tris.md2 ...x: -384.000000 ...y: -544.000000 ...z: -88.875000
Is in front of player: 0
Is visible to player: 0
Entity looking at player: 1
drawing entity: models/objects/barrels/tris.md2 ...x: 136.000000 ...y: 40.000000 ...z: 0.000000
Is in front of player: 0
Is visible to player: 0
Entity looking at player: 0
drawing entity: models/objects/barrels/tris.md2 ...x: 184.000000 ...y: -24.000000 ...z: 0.000000
Is in front of player: 0
Is visible to player: 0
Entity looking at player: 0
drawing entity: models/monsters/soldier/tris.md2 ...x: -281.000000 ...y: -61.125000 ...z: -23.875000
Is in front of player: 1
Is visible to player: 1
Entity looking at player: 1
read 4 bytes: test
```

We can see by looking at `models/monsters/soldier/tris.md2` that the enemy solder we see in the screenshot is in front, visible, and looking at the player. You can also look at the data capture for the two stim packs in the scene of the right side, `models/items/healing/stimpack/tris.md2`. Both are visible and in front of the player. The barrels mentioned as `models/objects/barrels/tris.md2` that are behind the player and on an elevated position are neither visible or in front. This last piece requires a bit of map knowledge of `base1` to confirm.

The last piece, `read 4 bytes: test` is the received message from the python launcher program upon receiving data for the current frame. Currently this is just `test` but it can be some indicator telling the game what to do next. We will discuss this more in next section.

Along with frame data we also capture sound data which is demonstrated below:

```
Player heard sound: infantry/infatck3.wav ...x: 20.000000 ...y: -56.000000 ...z: 248.000000
Player heard sound: world/spark6.wav ...x: -360.000000 ...y: -210.000000 ...z: -2.000000
Player heard sound: world/flyby1.wav ...x: -824.000000 ...y: 912.000000 ...z: 472.000000
Player heard sound: world/spark5.wav ...x: -460.000000 ...y: -38.000000 ...z: 8.000000
Player heard sound: world/spark5.wav ...x: 20.000000 ...y: -56.000000 ...z: 248.000000
Player heard sound: world/spark7.wav ...x: -456.000000 ...y: -12.000000 ...z: 52.000000
Player heard sound: world/explod2.wav ...x: -2056.000000 ...y: 1560.000000 ...z: 216.000000
Player heard sound: world/spark7.wav ...x: 124.000000 ...y: -296.000000 ...z: 264.000000
Player heard sound: #players/male/pain25_2.wav ...x: -1048.000000 ...y: 600.000000 ...z: 104.000000
Player heard sound: soldier/solatck1.wav ...x: 124.000000 ...y: -296.000000 ...z: 264.000000
Player heard sound: world/flyby1.wav ...x: -1224.000000 ...y: 40.000000 ...z: 72.000000
Player heard sound: infantry/infatck3.wav ...x: -1224.000000 ...y: 40.000000 ...z: 72.000000
Player heard sound: #players/male/pain25_2.wav ...x: -456.000000 ...y: -12.000000 ...z: 52.000000
Player heard sound: soldier/solatck1.wav ...x: -1048.000000 ...y: 600.000000 ...z: 104.000000
Player heard sound: #players/male/death2.wav ...x: -1224.000000 ...y: 40.000000 ...z: 72.000000
Player heard sound: infantry/infatck3.wav ...x: -1224.000000 ...y: 40.000000 ...z: 72.000000
Player heard sound: world/spark7.wav ...x: 20.000000 ...y: -56.000000 ...z: 248.000000
```

The data above is pretty self-explanatory, it's the sound being made and location where it is being made. The sounds in the log shown above correspond to what's happening in the scene.

### Sample of Message in a Given Frame

![quake 2 awareness demonstration 2 gif](https://raw.githubusercontent.com/CanntAim/yquake2-bot/grabbing-game-state-sockets/gym/documentation/peek_quake_q2dm1.gif)

We could pass all this information to Python launcher program but it's arguable whether this is necessary. What we instead do is have features that summarize information that is relevant to us in the scene. The current feature set is what exists in the `Message` that we update on each frame. This is subject to change but as a first go it is sufficient. A sample of what the launcher program receives is shown below:

```
['1267.562500', '690.562500', '494.187500', '11.019287', '-41.253662', '0.000000', '0', '0', '-1', '1525.717041', '-99999.000000', '-99999.000000', '-99999.000000', '-99999.000000']
```

As you can see this maps to what we see in our `message_t` struct shown previously and shown below. Values that are `-1` or `-99999` are field values that never have been initialized. For example, if a player never saw an enemy player yet the `enemyPosition` fields will stay `-99999`.

```C
typedef struct
{
  float playerPositionX;
  float playerPositionY;
  float playerPositionZ;
  float playerViewAngleYaw;
  float playerViewAnglePitch;
  float playerViewAngleRoll;
  int time;
  int frags;
  int enemyLooking;
  float enemySoundDistance;
  float enemyPositionX;
  float enemyPositionY;
  float enemyPositionZ;
  float projectileDistance;
} message_t;
```

## IPC between Quake and Python

As has already been mentioned we will launch Quake 2 from Python and will have our `cl_agent.c` feed information that was captured in the game back to that launcher Python program. The reason we want to do this is because Python is a more convenient language to apply our reinforced learning techniques given availability to packages like openAI Gym. That said we want the IPC between the game and Python launcher to be quick. We developed our Python launcher with the requirement that it can run on the same device as Quake meaning we can use Unix domain sockets.

We will now examine the code responsible for establishing a communication channel between the game and Python. The launcher takes the following parameters:

* socket - path to UNIX socket. (default `../../quake_socket`)
* path - path to where quake 2 is. (default `../../release/`)
* host - whether this client is host or not. (default `y`)
* address - address of server to connect to. (default empty)
* port - port of server to connect to. (default empty)
* episodes - number of episodes. (default `10`)

By default the launcher will launch the game as a host and allow other local network clients to connect to it. If host is not `y` then `address` and `port` need to match the address of a started host client and the port on which Quake 2 is listening on that device. You **cannot** have two clients connect to one another on the same machine.

Upon calling the Python program the launcher will start Quake 2 as a sub process, it will wait 10 seconds for the game to startup after which it opens a socket and connects to it. If the launcher had `host` option set to `y` it will send a `start server, localhost` message to the socket. We will talk about how the game listens on that socket a bit later. If the launcher is a peer trying to connect to the host or its `host` option is not `y` then it will send a `connect to server, <address>:<port>` message to the socket.

For the game to receive messages it also needs to connect to the socket and read messages from it. On the first in-game frame quake will run `GymStartServer` which will create a thread that opens a connection to the Unix socket along with initialize the global instance of `message_t` called `Message`. Upon opening a socket connection it will set a flag that it has done so and will start listening on that socket in the thread we previously created. On the main thread it will run the game logic which includes sending messages back to the Python launcher as previously shown.

With this we have a way for the game to send messages to the launcher and have the launcher respond and a way for the launcher to send messages to game and have the game respond. We now have a working environment that is representative of this model.

```
                                     *----------*
                *------------------->|   Agent  |-----------------*
                |     *------------->*__________*                 |
          state |     | reward                                    |  action          This is describtion
           S_t  |     |   R_t  R_t+1                              |   A_t            of standard RL model.
                |      ----------*---------------*                |                  where agent-environment interaction is defined as such
                *----------------|  Environment  |<---------------*                  in a Markov Decision Process.
                           S_t+1 *---------------*
```

For the next iteration of the project we can focus on the reinforced learning component more than the technical element of how to extract information from the game. Every game requires two bots playing against one another thus there will always be an agent that is a host and an agent that is a connecting player. To test the code we have been using [QubeOS](https://www.qubes-os.org/) as a test platform which is security focused OS based on the Xen OS hypervisor. We set up two qubes on the virtual network and open both of them up to communication with  one another.

![quake 2 awareness qubes demonstration 1](https://gitlab.com/Cant_Aim/blog/raw/master/quake_2_demo_awareness_system_vms.png)

Above we show the qube manager that has some basic information on each qube. Below we have a screenshot of the game running in the two qubes that we have setup to test the network code between host and connecting peer. 


![quake 2 awareness qubes demonstration 2](https://gitlab.com/Cant_Aim/blog/raw/master/quake_2_demo_awareness_system.png)

It's same as the demonstrations shown above on an _Ubuntu_ box except here they run in virtual machines or _qubes_. For future iterations we will implement a testing environment using a type two hypervisor which is more accessible to regular Linux users. If you wish to review changes more closely or try the _WIP_ version feel free to go the `grabbing-game-state-sockets` branch on [this](https://github.com/CanntAim/yquake2-bot/tree/grabbing-game-state-sockets) fork of _yquake2_.
