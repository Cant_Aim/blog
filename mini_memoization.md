---
title: Mini-notes: Memoization 
taxonomy:
    category: blog
    tag: [code tricks, computer science, dynamic programming, memory, performance, optimization, notes]
---
<p><b>Memoization</b> Is a fairly common trick that is used to get performance benefits out of certain algorithms that make repeated function calls through out their execution. If you ever wrote a dynamic programming algorithm (for knapsack perhaps) you are most likely familiar with the concept. This is somewhat similar to the idea of caching. In Layman's terms you can think of it like this, assume I want you to calculate the target sum of a range {1 to 10}, This will most likely take you some time, _Assuming you don't do this the analytical way <i><b>10*(1+10)/2</b></i>:</p>
===

_1+2+3+4+5+6+7+8+9+10 = 55_

Either way you did it though, if I were to proceed to ask you what's the target sum of a range {1 to 11} then you are most likely to immediately say 66. Voila! you just did memoization.

<h6>Example</h6>

As an example I will use the following problem.

> _Assume we have a direct mapping of letters to numbers: A maps to 1, B maps to 2, C maps to 3, so on and so forth until we have Z which maps to 26. We can use this mapping to produce an encoding of an input ABC to 123. We can decode this to multiple ways though: ABC, LC, or AW. The goal is to produce an algorithm that tells us the number of possible decodings_

The regular recursive solution to this problem is straight forward.

```python
def num_of_possible_decoding(self, input):
    m = [None for i in range(len(input))]
    return self.count(input)

def count(self, input):
    if not len(input):
        return 1
    result = self.count(input[1:])
    if len(input) > 1 and int(input[0:2]) < 27:
        result += self.count(input[2:])
    return result
```

This solution simply proceeds through making all possible selections and counting up the result when we successfully decode. We demonstrate the execution below:

|abc   |abcd    |abcde    |abcdef      | step |
|------|--------|---------|------------|------|       
|a-b-c |a-b-c-d |a-b-c-d-e|a-b-c-d-e-f |**1** |                
|a-bc  |a-b-cd  |a-b-c-de |a-b-c-d-ef  |**2** |
|ab-c  |a-bc-d  |a-b-cd-e |a-b-c-de-f  |**3** |
|      |ab-c-d  |a-bc-d-e |a-b-cd-e-f  |**4** |
|      |ab-cd   |a-bc-de  |a-b-cd-ef   |**5** |
|      |        |ab-c-d-e |a-bc-d-e-f  |**6** |
|      |        |ab-cd-e  |a-bc-de-f   |**7** |
|      |        |         |ab-c-d-e-f  |**8** |
|      |        |         |ab-cd-e-f   |**9** |
|      |        |         |ab-cd-ef    |**10**|   

The table above demonstrates our execution steps of the algorithm for input of size three to six. And each execution step requires us to go through the entire input string.

| Input Size | Execution Steps |
|------------|-----------------|
|**3**       |_**3**_          |
|**4**       |_**5**_          |
|**5**       |_**7**_          |
|**6**       |_**10**_         |

As you can see we are seeing an exponential increase in our runtime with each additional character thusly the run time is _**O(2^n)**_. One immediate thing to notice is this is a [Markov](https://en.wikipedia.org/wiki/Markov_chain) process. If I'm at character _**x**_ and I have discovered that it's possible to produce _**y**_ decodings then there will be at least _**y**_ decodings no matter what decodings I chose for the characters _ahead_ of _**x**_.

Also, you should notice that we track over previous steps unnecessarily, take the following input, _abcdef_, at the end of step 1 we count up at _least_ one way to decode the message. We next go back and see if we can replace integer 5 and 6 with single letter. This is still fine, but then look at step 3. We replace integer 4 and 5 with a letter and then we proceed to check if character 6 can be replaced by a letter even though we know we can from the previous call to count with the same exact parameters.

To fix this we can _cache_ or _memoize_ the values we have previously calculated, thusly the algorithm becomes this.

```python
def num_of_possible_decoding(self, input):
    m = [None for i in range(len(input))]
    return self.count(input, 0, m)

def count(self, input, k, m):
    if not len(input):
        return 1
    if m[k] is not None:
        return m[k]
    result = self.count(input[1:], k + 1, m)
    if len(input) > 1 and int(input[0:2]) < 27:
        result += self.count(input[2:], k + 2, m)
    m[k] = result
```

<h6>Optimization</h6>

The array _**m**_ now stores our previously calculated result which we can reference whenever we parse over an element of the input we have looked over previously. We fill out the array _**m**_ as we recursively make our choices. The second return statement though _"blocks"_ us from diving further in our recursion then we have to. If we know the value up to this point was _**y**_ at position _**x**_ we aren't going to waste time re-asserting that fact. This reduces the time complexity of the algorithm to _**O(n)**_.   

The execution of the algorithm for input size of 6 will look like this now, where we pass off knowledge collected from the previous step to the next step. We spend our time only checking if the two integers of this current iteration can be decoded into a letter and adding to our result if it can:

|abcdef      | step |
|------------|------|       
|a-b-c-d-e-f |**1** |                
|a-b-c-d-ef  |**2** |
|a-b-c-de    |**3** |
|a-b-cd      |**4** |
|a-bc        |**6** |
|ab          |**7** |
