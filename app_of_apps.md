---
title: Application of Applications 
taxonomy:
    category: blog
    tag: [actor models, rpc, distributed, memory space, architecture, design]
---
<p>Most modern services, platforms, and systems consistent from a number of different programs/processes that communicate amongst each other. This is a very different notation from traditional applications that do everything in a single domain. Modular design is important in all programs for many reasons including; security, maintenance, speed of delivery, performance, etc. This separation of concern can be done at different levels and in a number of ways. There is ultimately no one sure fire way to do this "correctly" and there are many different proposals from engineers and computer scientists about methods to doing this. From the historical context there are programming paradigms, system design discussions, and technologies that have pushed this design principle forward. Structural (object oriented, object based, etc), Micro kernel vs. Monolithic kernel, Unix Philosophy and Actor Models are all important topics tied to this discussion. Making the right choice in design is less of a science and more of an art that comes from experience in building systems.</p>
===
## Understanding Memory Space

<p>In a classic application you will have an entry point (for example a static main method) that will serve as a jumping off point to execute the methods of object instances or static classes that live somewhere on a heap. The stack will build up as we make calls and empty as we return from those calls. The stack itself is composed of frames that help give our functions scope with any parameters and locals being stored and accessible only in the context of those frames. This memory layout gives us an opportunity to separate data inside of a program's memory. As software engineering progressed we started seeking to add more of this type of separation in program memory. So we introduced concepts like classes and access specifiers that gave us separation in the heap. A stronger guarantee of memory isolation, though, occurs between processes themselves. A basic principle of operating system's dictates that any two processes logically cannot share data, i.e. a process does NOT know that another process exists. Fun fact, is that the recent CPU exploit in the news, Spectre and Meltdown, uses a hardware bug that breaks this guarantee. The side-effect of this is that from the stand point of any process running on your computer it has a practically infinite memory space (i.e. the virtual memory space) to work with. Off course this is not true as RAM is a limited a resource and the physical memory space needs to be shared between many on-going processes. This is where the OS does page swaps to move data from hard drive to RAM and vise-versa giving the illusion of a much larger memory space.</p>

```
                                                DATA
                   _________STACK_________                         _________HEAP_________
                   |                     |                         |     string pool     |
                   | foo()               |                         |   |             |   |
                   |---------------------|        |----------------|---|-str_object  |   |
                   | params: str         |<-------|                |   |             |   |
                   |---------------------|                         |   |-------------|   |
                   | locals:             |                         |                     |
                   |---------------------|              |----------|--------objA         |
                   | return address      |              |          |                     |
                   |---------------------|              |          |                     |
                   |                     |              |          |                     |
                   | main()              |              |----------|--------objB         |
                   |---------------------|              |          |                     |
                   | params: objA,objB   |<-------------|          |                     |
                   |---------------------|                         |                     |
                   | locals:             |                         |                     |
                   |---------------------|                         |---------------------|
                   | return address      |
                   -----------------------
```

<p>When talking about constructing platforms it's also useful to understand Static/Dynamic library linking as this is important to the way in which programs are inherently built. This is a topic that is less often talked about when building applications but is useful to understand. There are two ways to use a library's code in your application. The first way is to have your compiler/interpreter make a copy of the library that will then be contained in it's own memory space. This will use more space and make re-compilation necessary. Dynamic linked libraries on the other hand will share the code block between process that need it. Note, that the data block is never shared as this would break the memory isolation principle we talked about previously! More important is that breaking up our code in too packages and linking them during compilation, while giving us logical separation of work, doesn't give us a separation of state between the parent package and it's children packages (dependencies).</p>

## Concurrency and Lock Issues

<p>While memory isolation is an important part of making modern systems manageable, safe, and also easy to use it's also inevitable for a section of code in a program to share it's state with other pieces of code. This is where important system design questions start coming up like how to handle concurrent resource access. Operating Systems are programs that are highly susceptible to race condition issues since the main job of the OS is to manage the resource fetching for processes. Resources in this case could be hardware, file-system, and also other user-land processes in a time-sharing environment.</p>

```
Process 1 (runs for 10ms) -> Process 2 (runs for 20ms) -> Process 3 (runs for 5 ms) -> Process 1 (runs for 5 ms) -> ...
```

_Note: Time sharing is when processes take turns using the processor to execute bits of their code._

<p>Operating system designers have come up with relatively complicated ways to handle these complexities: For example, through special abstract data types, like semaphores, locks, and mutex that guarantee resource access is managed properly. The conversation often times resolves around subjects like multi-resource deadlock (see dining philosophers problem) and other issues that are intuitively hard to solve but very relevant to the design of any multi-process system.</p>

<p>As distributed systems (this includes multi-processor systems and  networked systems) have gotten more prominent additional concerns have come up with how to handle network consensus, atomicity, and linearizability -- these are subjects that will explored in my later write up.</p>

<p>After a while, many coders have had enough of having to worry about hard to discover memory related errors that slowed down development and started to look towards old ideas regarding how to avoid shared states in a distributed setting altogether. Skip circa 2010 and you start seeing a major resurgence in functional languages and other paradigms that lines up perfectly with network services and multi-core processors. The next section will explore two paradigms and explain practical differences between the two.</p>

## Object Oriented

<p>The object oriented model is often times the first flavor of structured programming that people get exposed to and for the most part it's by far the most prevalent. User-end tools (especially UI) are very convenient to build in this paradigm since it fits the typical problem statement of this category of applications. The idea is straight forward, you have object of classes that have variables and methods with access specifiers like private, public, protected. On top of that you can have strict contracts set on these objects through things like classes and interfaces. All these technical artifacts combined give you the three basic concepts that make up object oriented based programming: Encapsulation, Inheritance, and Polymorphism.</p>


```
Class Cat inherits Feline               Class Feline                              Class House

----------------------------------      ------------------------------------      -----------------------------------
| private Paw                    |      | private Paw                      |      | private Cat                     |
| private Tail                   |      | private Tails                    |      | private Owner                   |
| private Eyes                   |      | private Eyes                     |      |                                 |
| public  Ears                   |      | public Tail                      |      | public House(Cat cat):          |
|                                |      |                                  |      |  Cat = cat                      |
|                                |      |                                  |      |  Owner1 = Owner()               |
|                                |      |                                  |      |  Owner2 = Owner()               |
| public Cat():                  |      | public Feline():                 |      |                                 |
|                                |      |                                  |      | public lock():                  |
| public play():                 |      | private drink(Milk milk):        |      |                                 |
|                                |      |                                  |      | public unlock()                 |
|                                |      | private eat(Food food):          |      |                                 |
|                                |      |                                  |      |                                 |
|                                |      |                                  |      |                                 |
|                                |      |                                  |      |                                 |
|--------------------------------|      |----------------------------------|      |---------------------------------|

```

<p>Object Oriented features in languages like C++ are a big step up from other languages like C which lacks the components mentioned above, at the expense of a being more bloated solution. We will use the above pseudo UML to try and understand the advantages and disadvantages of the object oriented paradigm. Most important thing we want to explore is: </p>

* _Where our memory boundaries are._
* _Where there is potential for data exposure._
* _What are potential errors that can occur from this._

<p>The example above is rather clean and straightforward so it should be easy to identify the three mentioned points. Starting with bullet point one we can identify the data separation happening in several places. Firstly at the class level. Every instance of a class or a static class will have variables that are private and public. The access specifiers let's us control how much internals are exposed to the outside. Next at the function level we can have locals and arguments that are in their own scope (for now we are assuming we are passing-by-value). Additionally we could have scope in loops which is not shown here.</p>

<p>Secondly we can think about how these boundaries can be undermined. The easiest way to do this is simply through neglect. A developer working in an OO-based language can ignore data protection and simply make things global (i.e. have all data on the heap). This would mean avoiding classes or functions altogether. A developer can also be neglectful of access specifiers and expose variables by making everything public as well. Getters and Setters are the default way to maintain encapsulation by allowing controlled access to variables. There are less intuitive ways in which OO-design can be broken accidentally though. For example if you have an instance of certain class you might think that it's private variables are inaccessible in another class instance of the same class. This is actually untrue for certain languages like C# and Java. Some languages support class level privacy while others are instance based. A more common mistake is not knowing when you are passing-by-value vs. passing-by-reference. This could lead to bugs that are very hard to find. In case of a pass-by-value situation your argument could also be a shallow-copy or a deep-copy which adds an additional level of consideration when you are coding.</p>

<p>Finally we should think about the severity of these potential mistakes and how easy are they to make and then fix? The general consensus on side-effects is that they are mostly bad. Now, that doesn't mean that side-effects can't be useful. For example global counters, timers, loggers are a design choice in certain architectures. For example we may want an application X that does something internally to have a propagating effect to another part of the system. The question is can you keep you track of the propagation and control it? Any uncontrolled propagation is scary because it's your code doing something you aren't aware off meaning you and your collaborators are probably operating on assumptions that aren't true. Worse yet is the fact that side-effects can also lead to other side-effects meaning a single bug can spawn into a dozen other bugs in a large system. Side-effects are typically non-fatal (in the sense they will not cause an exception) but rather will cause semantic issues. Exceptions that do occur are typically bad though because they are unmanaged, because you didn't even know about them. In parallel or networked systems the concerns double. Sharing state correctly is tricky, especially in a large platform application that has dozens of modules. Experience and discipline here is key to making a successful project that isn't a tangled web of services that are hacked to work together.</p>


## Solutions to the Problem (App of Apps)

<p>The problems I mentioned above are those that software engineers and computer scientists have been struggling with for a long time. Today, software ecosystems are the hallmarks of successful businesses like Google, Microsoft, Apple, Amazon and Facebook. These companies and others have largely paved the way for how engineering of distributed services should be done. Most of this insight has been shared through publications and research. Many start-ups have also picked up these methods and techniques to avoid growing pains. Technical revaluations of your system down the road are hard to do and you want to avoid doing them. Preferably your system's extendability and manageability is independent from it's size or complexity.</p>

### Actor Models

<p>The first proposed solution, actor models, are foundational to languages like Elixir or SmallTalk. The concept of actor models was original proposed by [Carl Hewitt](https://en.wikipedia.org/wiki/Carl_Hewitt) and is an important point of discussion in computer science. The beauty of an actor model is it's pure simplicity that can be shown in a simple picture.</p>

```

-------------                            ------------                            ------------
|  Actor A  |                            |  Actor B |                            |  Actor C |
|           |             message        |          |           message          |          |
| Internal  |--------------------------->| Internal |--------------------------->| Internal |
|  State    |                            |  State   |                            |  State   |
|-----------|                            |----------|                            |----------|
      |_                                     _|
        |_                                 _|
          |_                             _|
            |_       -----------       _|
              |_    |  Actor C  |    _|
                |_  |           |  _|
                  |>|  Internal |<|
                    |   State   |
                    |-----------|
```

<p>At a high level this is pretty much it. The actor model is a dynamic distributed system that breaks down units of work and keeps them completely separate from one another. Think of it as an abstraction layer that ties everything together. Actors can exist in the same *logical* memory space but they will NOT share internal state. The real kicker though is that actors can also exist in completely different *physical* memory space and still be able to communicate with one another. To solidify this let's look at in-code example.</p>



```
Class Cat inherits Actor                Class Owner inherits Actor                Class House inherits Actor

----------------------------------      ------------------------------------      -----------------------------------
| private Paw                    |      | private Name                     |      | private Value                   |
| private Tail                   |      | private Salary                   |      | private Rooms                   |
| private Eyes                   |      |                                  |      |                                 |
| private Ears                   |      |                                  |      | public House():                 |
|                                |      |                                  |      |  Owner = owner                  |
|                                |      |                                  |      |  Cat = cat                      |
|                                |      | public Owner():                  |      |                                 |
| public Cat():                  |      |                                  |      |                                 |
|                                |      | private on_receive_give_water(): |      | public on_receive_lock():       |
|                                |      |  cat.send("drink")               |      |                                 |
|                                |      |                                  |      | public on_receive_unlock():     |
| private on_receive_drink()     |      | private on_receive_give_food():  |      |                                 |
|                                |      |  cat.send("eat")                 |      |                                 |
| private on_receive_eat()       |      |                                  |      |                                 |
|  owner.send("thank")           |      | private on_receive_thank():      |      |                                 |
|--------------------------------|      |  house.send("lock")              |      |---------------------------------|
                                        |----------------------------------|

```
<p>While these actors can be initialized on the same machine (aka same physical memory space), they don't need to be! In fact these actors can communicate amongst each other over a network. A key feature of actors is that they are location transparent meaning that their location is addressed and they are presumed reachable through that address. One valid question is who sends the first message in the actor model system? The answer is typically an adhoc signal by a user from browser or an admin from a terminal. When the system starts though it can take on it's own life and exclude all user input altogether. An never-end cycle of actors talking to one another, more likely though is that users will want to interface with the system in some way and send additional requests to actors to get work done. If you are comparing the object oriented diagram to the actor model you will notice another interesting detail; Control is inverted! Instead of an instance of cat invoking it's method, another object out in the void is able to invoke that cat to drink or eat. In this particular example we aren't to focused on details of the implementation but it's worth noting that the signal is typically not a string but a type. This type can be thought of like a packet. It has a header (from whom it came and to whom it went) and it has content (information we want to pass on)</p>

#### Advantages

<p>There are several important advantages to actor models:</p>

* _Code is side-effect free (state change in one actor cannot change state of another)._
* _Code is broken up into discrete units of work (An actor can/should have one responsibility)._
* _Code is easy to unit-test (We build test cases around sending messages to an actor and examining the response)._
* _Coding effort is easy to distribute (Because units of work are self-contained in actors)._
* _Code is easy to prototype. (Actors can be built independently of the other things being worked on)._

#### Disadvantages

<p> As anything in life, actor models are not a silver bullet. They are great for many things but they also have draw backs.</p>

* _First, in our case, it's an additional layer of complexity. It's necessary layer, but it's a layer none-the-less._
* _Second, is that execution happens asynchronously. In a platform composed of multiple modules communicating this is unfortunately unavoidable, actor models simply give that communication form and structure. But it does mean you need to account for additional exceptions like time-out._

### Network Layer(s)

<p>The Actor model example above is still in the same logical memory space. While we now do have stricter boundaries enforced by a framework (built in OO) there are still ways to break this, albeit it's a lot harder. So the question becomes can we make state sharing completely infeasible? Yes. In fact actor models are really not a novel idea at all. A nice quote from Robert Milner:</p>

>Now, the pure lambda-calculus is built with just two kinds of thing: terms and variables. Can we achieve the same economy for a process calculus? Carl Hewitt, with his Actors model, responded to this challenge long ago; he declared that a value, an operator on values, and a process should all be the same kind of thing: an Actor. This goal impressed me, because it implies the homogeneity and completeness of expression ... But it was long before I could see how to attain the goal in terms of an algebraic calculus... So, in the spirit of Hewitt, our first step is to demand that all things denoted by terms or accessed by names--values, registers, operators, processes, objects--are all of the same kind of thing; they should all be processes.

#### RPC

<p>Processes (applications running on your computer) can behave same as actors with the addition of a communication protocol between them. Thankfully, such a protocol already exists, it's called RPC. RPC stands for Remote Procedural Call, and as the name suggests it's a way to do remote method invocation across an address space. We can do all sorts of fun things like take an array of strings declared in Java and pass it to a method in Python that accepts an array of strings. Just like actor models we can use RPC to built distributed services running across different applications, but in addition those applications don't need to use the same language or framework.</p>

#### REST

<p>A more recent trend has been the use of REST (Representational State Transfer) which can also be used to achieve similar effect using HTTP. This is applicable to client-server models where the server operates without any context of what clients are doing. The reverse is true for clients who operate without any knowledge of the server's internal state, The clients simply send message to the server which causes the server to change it's internal state. A client, for example, can send a message with credentials for authentication which causes the server to alter it's state to indicate that the user logged in (generate and save current session token).</p>

