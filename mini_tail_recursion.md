---
title: Mini-notes: Tail-Recursion 
taxonomy:
    category: blog
    tag: [code tricks, computer science, recursion, memory, performance, optimization, notes]
---
<p><b>Tail Recursion</b> is a powerful yet simple optimization technique for whenever you have to do deep recursion. The problem with deep recursion is that you rely on maintaining a constantly growing call stack as you dive. This was a problem, twenty or thirty years ago, when memory was a real constraint (maybe only a couple of kilobytes could realistically be allocated to the stack). Now-a-days the typical stack size a compiler will support by default might be something like 4 megabytes and often times can be configured to be <i>much much</i> larger, like in the case of Java:</p>
===

`java -Xss2048m <program to run>`

<p>Off course this is an icky and naive solution at best and something less brute force would be totally welcome, enter <b>Tail Recursion</b>. Let's compare the following two cases that demonstrate the difference.</p>

<h6>Regular Recursion:</h6>

<i>Note: example borrowed from Stack Overflow.</i>

```
int g(int x) {
  if (x == 1) {
    return 1;
  }
  int y = g(x-1);
  return x*y;
}

```

<p>In the following, our function <b><i>g</i></b> defines a base case when <i>x</i> is equal to 1 and then proceeds to make a recursive call to itself. At this point we add a frame to the call stack. The frame will be composed of the input, locals, and returns of the newly called function (The frame gives us our method scope). But wait, hold on! We still aren't done with setting the return of the previous frame! We can't because we need <i>y</i> to be set, but for <i>y</i> to be set the newly created frame needs to set it's return and the function <b><i>g</i></b> that frame will call will need to set the return inside it's frame; so on and so forth until the base case. Once the base case is hit we can begin collapsing everything back to a single frame and garbage collect the ones we added for calculation. This is primed for a stack blowout if we get overzealous.</p>

<h6>Tail Recursion:</h6>

<i>Note: example borrowed from Stack Overflow.</i>

```
int f(int x, int y) {
  if (y == 0) {
    return x;
  }
  return f(x*y, y-1);
}

```
<p>The following example starts of same as the previous one with defining a base case but then does something slightly differently. It makes recursive call to itself but this time passes off the result of the final calculation directly into the call. Bingo, that's it, this simple maneuver now let's us do an inductive dive that can be infinitely deep.</p>

<h6>Optimization (The Magic)</h6>

<p>So... What's the magic behind the trick? Simply put we can now reuse the same frame over and over again instead of having to put a new one on the stack. Since the result is passed off in the recursive call there is no need to return back up the stack to collect our bounty once we hit the base case. The single caveat to keep in mind is that the success of this trick depends on how smart the compiler is as some languages don't support tail recursion optimization. But this is a tool you should keep in your arsenal if you plan on doing high performance coding. Also, note that some problems can't be solved with tail recursion as they rely on making a secondary recursive call after the first. The classic example of this is Fibonacci Sequence calculation.</p>
