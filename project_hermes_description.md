---
title: Hermes (Legacy)
taxonomy:
    category: blog
    tag: [Messanger, Security, Frontend, UX, Kivy, Python, Cryptography]

---
Hermes is a messenger developed by the Uconn Security club. It's currently solely a python application. My current work on it is primarily on the front-end client application. I used the kivy library for UX/UI development. The seperating factor between hermes and other messaging applications is that it doesn't only ensure end-to-end encryption but also the anonymity of the participants of a given conversation, to those outside of it.

===

In the coming months I will outline the details of how the core-api works and will also give some insight into the UI/UX development using kivy as well. To access the already available documentation, you can visit the project [github page](https://github.com/Abraxos/hermes).


