---
title: About
---
<h1>About</h1>

<h3>Introduction</h3>

<p>**Cant_Aim** is an alias I have been using online for quite some time now. My aim, no_pun_intended, is to keep this less about academic and industry work and more about hobby projects and personal ventures in the open source domain. That said, you can find easily find out more about me through my GitHub and also on Twitter. I'm not trying to be a 'Satoshi Nakamoto' or anything by using an alias on this site. What you might want to know is that I have a Master in Computer Science and my work experiences deal primarily with coding and technology.</p>

<p>I have spend time in multiple domains using various language and tools. On the more theoretical end I have done work with computer vision in the context of traffic monitoring, data mining in the area of bioinformatics. Some technical skills that I have developed include: web/full stack development, C/C++ coding, functional, scripting, system architecture, operating systems, network coding, security, etc.</p>

<h3>Technical Interests</h3>

<h5>Languages Currently Using</h5>
* <b><i>Python</i></b>
* <b><i>Javascript</i></b>
* <b><i>C/C++</i></b>

<h5>Languages Previously Used</h5>
* <b><i>SML</i></b>
* <b><i>R</i></b>
* <b><i>Matlab</i></b>
* <b><i>SmallTalk</i></b>
* <b><i>MIPS Assembly</i></b>
* <b><i>Ruby (on Rails)</i></b>
* <b><i>Java</i></b>
* <b><i>C#</i></b>

<h5>Topics</h5>
<p>I'm currently interested in optimization based problems and visual gesture recognition, I also have an additional interest in graphics programming and distributed systems. The sick3 project (visit the projects section) will explore use of Convolution Neural Net (CNN) in the context of continuous gestural movements. I also like spending time understanding other theoretical areas related to computer science. In the articles section I'll include write ups on my research in specific areas. I constantly aim to improve my coding standards and practices using software engineering patterns and techniques. Project write-ups will highlight things that I learned as I worked through problems.</p>

<h3>Other Interests</h3>
<p>I practice and have an interest in football freestyle which is a 21st century alternative sport founded in internet virality. I also enjoy gaming both competitively and casually. It's proubably the thing that got me most interested in coding as a kid. I've played games starting with DOOM II and have continued to keep an interest in them all the way till present day. I played competitive Counter-Strike for a time being and arena shooters have also interested me.</p>

<h3>Contact</h3>
<p>It's best to reach me on my private email although you are free to PM me on Twitter or on GitHub:</p>

<h6>Email</h6>
[Gmail](mailto:Ivan.V.Pozdnyakov@gmail.com) | PGP KEY FiNGERPRINT: 3055 FD07 4A50 C876 A596 63D3 378B 1262 FA77 C7C5</b> | [Download](https://cantaim.live/public.key)
> pgp --keyserver keyserver.ubuntu.com --recv-key 0xFA77C7C5

<h6>ProtonMail: If want secure comms but don't want to use PGP</h6>
[ProtonMail](mailto:Ivan.V.Pozdnyakov@protonmail.com)
<h6>GitHub: For questions related to projects</h6>
[CanntAim](https://github.com/CanntAim)
<h6>Twitter: For general garbage send a DM</h6>
[@Cant_Aim](https://twitter.com/cant_aim)
