---
title: Home Network Infrastructure
taxonomy:
    category: blog
    tag: [networking, security, home, systems, administration]
---

Having a well managed system or network is a cathartic experience and beneficial to your well-being. Ever have networked device not work as expected? Have you struggled understanding why said issue exists? An important part to being able to easily explain such issues is having a grasp of the context under which they occur. The best way to understand the context is by going through the process of creating it yourself. This generally means setting up your own internal and external facing services that you have direct control over. This also means knowing how to configuring the network properly. This post is about how I have my personal systems and network setup. I will review what services I run and how I run them. I will also review broader issues around how I handle separation of concerns as well as the security and maintenance benefits I get from that.

## Introduction

I will break down this post down into several sections. The first section will be about my primary and secondary network. Then for each sub-net of my networks I will talk about what services and devices I have on the sub-net as well as the properties of that sub-net. After that I will dive into details of particular devices and services. This post will be less focused on theory and pure technical system management.

## Networks

I have two completely separate networks in two separate physical locations. I will talk about the primary one here as the secondary one is less interesting although I may cover it in a future post.

### Network Infrastructure

The primary network is my main network and the one I spend most time on. This is the one that is most involved in its setup. The network has 3 sub-nets: From now on I will refer to these sub-nets as Home, Isolated, and Guest. The entry point to this network is _1200AC_ router running _dd-wrt_ with _brainslayer_ firmware. The configuration of the router is 4 access points:

* Home 5Ghz
* Home 2.4Ghz
* Isolated 2.4Ghz (VLAN) AP and Net Isolation Enabled
* Guest 5Ghz (VLAN) AP and Net Isolation Enabled

The Isolated and Guest sub-nets are virtual LANs that are partitioned off from the rest of the Home network and one another. The Isolated and Guest Network both have AP and NET Isolation enabled. This is so that devices on those VLANs cannot see each other. AP Isolation drops all traffic between clients connected to the VAP. This is recommended to prevent wireless snooping attacks as well as attacks that require direct port access. Net Isolation creates firewall rules blocking certain devices from the home network. Net Isolation works only on an unbridged interface on newer dd-wrt builds. Basically, devices on Home can talk to one another, devices on Isolated cannot talk with one another nor anyone on home as well as anyone on Guest. Guest and Isolation use different WPA2 keys as guest is meant for devices I don't own. 

Isolated and Guest has DNS setup to use OpenDNS but there is no network firewall. For the Home Network we use an internal DNS proxy that will also filter out domains associated with tracking and malware. Since Guest is meant for devices I don't own I'm okay with not requiring this protection. The isolated network, as you will see, primarily has servers, burners, and "carry-around" devices. Servers don't make a lot of requests out and my "carry-around" and burner devices have other means of protection that I will talk about later.

The router also runs an OpenVPN service that is capable of putting devices onto my Home network at which point that device has access to all my internal facing services including the network firewall since the VPN configuration forces the device to use my DNS proxy. An interesting note about the OpenVPN service is how I authenticate with it, which is through keys. This is worth remembering when we get to talking about the Local Certificate Authority. There are other configuration details that I have left out about things like DNS rebind settings or attachment of MAC address of requestors to DNS requests but these are minor. 

### Primary Systems and Services

We will now talk about systems and services that live on the network, starting with client devices and then moving up to services. Once you get the through this section the ascii diagram below should make a lot of sense.

```
                    Isolated            Home            Guest
                |--------------|-------------------|-------------|
                |    Burner    |    Chromecasts    |             |
    Linode      | Carry Around--* Google Home/Mini |    Stuff    |
  ----------    |              ||  Linux Desktop   |      I      |
  | Aesop<-|----|->  Icarus    ||   Win Desktop    |    Don't    |
  ----------    |     Helm     ||     Pixel C      |     Own     |
                |              ||      Xbox        |             |
                |              |*-> Carry Around   |             |
                |              |      Daedalus     |             |
                --------------------------------------------------
```


#### Client Devices

There are three categories to devices on the network that are classified as _home_, _carry-around_ and _burner_. I will explore each category here and talk about common tools these devices use.

![Clients](https://lh3.googleusercontent.com/unUp5-MnHBkt1rE8E3_xhPUNen9SwTWKPlKl2MRSdVdVcLLmTNvy3TTe2y5TE92Zl6BGuh2cOTSdG7Gp70kjibf7H-NUoX-cAtjNjKGt_y7Xw8dYpyjb7btSjG58EmyX5MzqKoc46gjs7pMsXYnOPOPD16RXiK5Fip4x0GgLtYvPJpcKajdoIK-h-QfzxAeHKUaoNGA53EMQ0GGmJJcBUFic2xJOXFVIW0a1-dk9bduR4RdW1ZPS9NHTTqe8mCKBC1tkoLl4q9nRRXAU2dRNdA28yJOsfgycgZE4Qv5NSNp4aSlU19_6zChrcFCMHIIwuWRkHWWLhC1g8hovGP1vYWaF5lSk90JV1k55mOuM8YIy1MZm1xy4jQZeMpNgLN7-t_2cds0RblQCo1qchWt0aQfvyZh2aTvqXlOKwIeJBoaQkFooXTh3lRcedob3uv-YtoAAVDSsQkRc04v15gD73itKlB7dmSYnGNRPcOixS8LMcLRixvn0EHlzE_BQJI_jdtI0RIM_IJwdwFRK5sAmQzmoWqs3lF4ADtb9wixOjbPnEnd-RRIjAnn8ii_GFOeH8GS4ai4qMgUozs8GYWzPbfFup0HvH_kfetVe8Il-q0GJP9nj429wSCVv7DT4mB_hKcTvakJtqJraMjlwjjazSOUgmp-8Sp2tSrVGCeRmrXs6HmcBbaQynojdnFEU4FlMAiKGNijaQJV00w3Xc2VBA9ZQ08M7cjaBWqZzWTRbEvvhokeg=w1268-h951-no)

##### Home Clients

Home clients are those that stay on the network or rather the Home subnet and don't move to other networks and cannot be accessed directly from the internet as they are behind a NAT and no ports are forwarded to them. A list of these devices includes the following:

* Linux Desktop
* Windows Desktop
* Google Home/Mini
* Chromecasts
* Steam Links
* Pixel C
* Xboxs

Both desktops use PIA VPN although it's configured to use my local DNS Proxy rather than PIA's DNS server. The PIA client is also configured to have local network access which by default is blocked. The Windows machine is set to treat the network it's on as a public one which causes Windows to use stricter network discovery rules. The Windows machine also runs an instance of Synergy which allows it to share it's mouse and keyboard with the Linux Desktop. The Windows machine also has Telegram, Thunderbird, GPGWin, and Kleopatra for Email and PGP key management. Since the Windows machine is meant strictly for gaming it has no further utilities.

The Linux Desktop on the other hand has a number of extra utilities that leverage the magic of Linux. Besides Telegram and Thunderbird with Engimail plugin it also runs client software for storage services. This includes an instance of Syncthing for distributed storage between all my devices and a Nextcloud client. For system monitoring there is [Glances](https://nicolargo.github.io/glances/) and [OpenSnitch](https://github.com/evilsocket/opensnitch) for real-time connection monitoring. I use [Synergy](https://symless.com/synergy) client for getting access to the shared Windows desktop mouse and keyboard. For password management there is [Pass](https://www.passwordstore.org/) (UNIX password manager) and other more commonly used Linux utilities like GPG. For automatic snapshot back-up I use a tool called [Timeshift](https://github.com/teejee2008/timeshift) which is scheduled to do monthly saves of restore point to the HDD.

The Google assistants and chromecasts are self-explanatory. Configuring your Google account to be more privacy conscious is a whole separate conversation but one important thing you may want to check on these devices is that guest mode is disabled. Otherwise people not your network will be able to access the device just by being in the same room as them. As stated on the Google support page:

>Chromecast emits a special Wi-Fi and Bluetooth beacon. When a Chromecast-enabled app is launched on your guest's mobile device, that device detects the presence of the special Wi-Fi or Bluetooth beacon and shows the Cast icon in the application. Upon tapping the Cast icon, casting to a 'Nearby Device' will be listed as an available option. Your Chromecast then generates a random 4-digit PIN that is required to cast to it using guest mode. When a device nearby tries to connect, the Chromecast automatically transfers that PIN using short, inaudible audio tones. If the audio tone pairing fails, your guest will be given the option to connect manually by entering the 4-digit PIN found on your Chromecast Ambient mode screen and in the Google Home app.

The Pixel C is my most commonly used device. Since Google has ended support for it last year I have flashed it with LineageOS so that it keeps getting security updates. Just like my desktops it also uses PIA VPN with DNS set to my proxy and LAN access enabled. It also has Syncthing, Nextcloud, and Pass clients. For key management on Android I use OpenKeyChain. [OpenKeyChain](https://www.openkeychain.org/) has a number of useful features like the ability to transfer keys securely over wifi. Instead of Thunderbird I use K-9 Mail. On my devices that are not Android I have Thunderbird configured to access my Gmail as well as Helm (Private email server). On Android [K-9 Mail](https://k9mail.github.io/) _only_ accesses Helm as Gmail already has its own app. Some other recommendations I can make around using Android are things like setting up F-droid as an alternative Package/Application manager. F-droid is open-source and has a curated repository with other open-source software. Additionally, I have [Termux](https://termux.com/) on all my Android devices being that Android is Linux it's only natural to have a terminal available.

##### Carry-Around

My carry-around devices includes phones and laptops that I take with me. They are devices that might potentially find themselves on other networks. These devices just as all my other ones use PIA but unlike those devices they are specifically configured to use PIA DNS and disallow LAN access. These devices sit on the Isolated VLAN whenever they are at home and only connect to the Home subnet when they need to sync the password manager. My main driver laptop is an XPS13 Dell with Ubuntu 18.04 LTS and it's configuration is the same as a desktop which runs PopOS 19.04 (A flavor of Ubuntu). My current phone is a Pixel 3 devices that also is configured the same as my Pixel C. The carry-around devices also have an OpenVPN client for connecting to my OpenVPN server. This serves as a quick way to get on the home-network when needed. The carry-around device in this instance gets to leverage my DNS proxy and all the advantages that come with that and other internal facing services.

##### Burner

Another practice I follow is the use of a burner device. It's inherently nice to be ableto test ideas without any of the potential security and privacy consequences of screwing up. Burner devices are useful for this sort of testing, but instead of having many physical machines what I instead do is I virtualize them using an type one hypervisor called QubeOS. QubeOS is great because it's based on Xen OS and carries a lot of the cool features of like Xen networking. Effectively what it looks like from the perspective of my desktop environment is that I have plethora of machines available to me on a network living on my one physical machine. 

My QubeOS box sits on my Isolated Network but the underlying "qubes" themselves live on a xen network where they are further isolated from any of devices on the VLAN and even between themselves. I can easily establish connections if I need to test some networked code/software from the `dom0`. QubeOS also distinguishes between what it calls _"AppVMs"_ and _"TemplateVMs"_. TemplateVMs form the basis for AppVMs and allow for changes outside of the home directory. An AppVM on the other hand will revert any changes made outside of home next time I spin it up. Here are some examples of qubes and their respective purposes:

* Work (Fedora-XX)
  * off-to-side software projects
  * npm utilities like minisign or hackmyresume
* Untrusted (Fedora-XX)
  * working with untrusted usbs and software
  * torbrowser
* Personal (Fedora-XX)
  * deluge
  * sftp
* Vault (Fedora-XX)
  * air-gapped
  * secrets
* Personal-debian (Debian 9)
  * same as personal but with debian instead of fedora
* Work-debian-test-1 (Debian 9)
  * for testing networked software
* Work-debian-test-2 (Debian 9)
  * for testing networked software
* Disposable
  * restarts completely fresh 
  
Here is a view from the `dom0` manager:

![Qubes](https://gitlab.com/Cant_Aim/blog/raw/master/home_network_qubes.png)

##### Extras

Some other less commonly used machines that I have include Kali Linux and Lakka although these are used far more sparingly as their use case is limited. Kali Linux, is a pen-testing box that has useful tools for diagnostics purposes. Lakka is an emulation platform for retro gaming.

#### Servers

Taking a DYI approach to services related to email, storage, password managers, network security, VPN and media playback might sound intimidating but it's actually quite do-able given the free time to do setup and the occasional maintenance. And while you might argue _"But I can have google or dropbox or netflix do it for me"_ there is an undeniable benefit in terms of privacy, security, customization and experience that is gained by taking the time to set these services up yourself. Most of these solutions are open source with decent size communities that can offer you support. Being open-source it also provides you the opportunity to contribute back and get involved. In this next section I will run down the servers I have and the services they provide.

![servers](https://lh3.googleusercontent.com/qf5ygIHot12ELBjbcmpUwC_D6VmnIDKV5fH0gDd2BYGhs6SR03Las6p0N9Lo-Tp9WfIOOvff-ztCpDzaApYU3R2ApuuOByb5ndzZIKQdCsoEcEcG_LyK-v9FxGv_jNr8ickbOQz1nr3sRVNEQ3-MuoOE6sJSHEImAWTedbP73X7xFH7s-N64y_ba84n_bGP9PuGxdTWbKKXHRxTtdHwwGLtc_pv-sorTcvzVU6hFBifLCgFyZF2RTiu3V_JVE8_SEcsGMULioTAPOL-FCAqZ14tMXWHau63vRPQ6WttYisO7BuwqFmFGmCDlRGWtor6HmrFYP5yPRAJ0aitmSIOWs8ApQQ7CmoxMl5btlQt-lndwO5J107uls17kd6wM3zvsdj_2LDNnyDrIa8Huw8WlS9boUqcmmadRadABNvYCgFSPKyxO0T80gAtme-TCwor9UXL5EPw9maCtl3NTFzHLI3me4UCSONpylDc3yM_ZQcodq8Potps1jSJFTiq6iJ0kqfv793JcdAdxjYDfE7D2mLbwQRJzAL7nowaarMH5r4_BKI10EdXWQyFs9pfgZ5-Np4SKt-pXuEdqYfTEybsXFYKpgUPWuAIdnh-sLFNmTuIvBAsUSICkpTu8jopCHH5qsl7tmMdyh-O11z08ESIlT5Bio8fGrVDwNoA-S1VNhYu4OIpjmuVOG5o-Gai8UDnwrEnwc_z9FPM1dEFqbdqZmsfOjNivqk-5jwzYrRezDNi8XUvX=w1268-h951-no)

##### Daedalus

Daedalus is internal facing server running on a Intel NUC, being an always on machine it nice to have it be a device that consumes a low amount of power. Daedalus hosts a number of critical services that are accessible only to devices on the Home subnet. These include, [DNSCrypt-Proxy](https://github.com/DNSCrypt/dnscrypt-proxy) and [Pihole](https://pi-hole.net/) which offers dnscrypt and DNSSEC to all outgoing DNS requests on the home subnet. This means that the ISP effectively cannot see what sites I visit from looking at the outgoing/incoming DNS traffic. Also, DNS responses are signed and can be validated that they are correct and haven't been altered. Pihole filters requests to tracker and malware URL. These are often times filtered by my browser plugins like umatrix and noscript but this adds an additional layer of defense. Along with that Windows, Xbox, and Google Home send constant telemetry data to their respective motherships, Pihole stops this.

![pihole](https://gitlab.com/Cant_Aim/blog/raw/master/home_network_pihole.png)

The other critical services is the pass git repository. This lets me sync my local devices' password managers with this central repository whenever I'm home. As mentioned before all my machines except Windows have Pass clients installed either as a terminal process or as an app in the case of a phone or tablet.

Daedalus also runs a Plex media server which provides media playback to the entire network as well as devices off network. Plex offers a nice relay service which means you don't have to port-forward to have it be available outside the network. For extended storage the NUC has 3 External Drives attached to it (4TB, 4TB, 6TB). These are using ext4 + LUKS.

Like all my other devices Daedalus is a Syncthing node and is part of what surmounts to a private cloud storage solution. Like my other devices it also has Glances which can be ran as server instance, meaning I can monitor live system resource usage from all my other devices on the network.

![glances](https://raw.githubusercontent.com/nicolargo/glances/develop/docs/_static/glances-responsive-webdesign.png)

For easy access from devices on LAN Daedalus has ssh setup with key authentication but Daedalus is inaccessible unless you are tunneled onto the network with OpenVPN.

##### Icarus

Icarus is an external facing server running on Raspberry Pi 3. Just like with Daedalus this device is always on and consumes a low amount of power. Being that it runs services that are explicitly out-warding facing there are extra security measures that I take. The two primary services that Icarus runs are Nextcloud and Grav. Nextcloud is centralized cloud storage solution, it includes features like calendars, video/audio playback, as well as file versioning. The data folder for Nextcloud lives on an external 4TB drive with ext4 + LUKS. The other service on Icarus is Grav which is an open source content management system. In fact this blog is run using Grav. Additionally, Icarus is accessible externally through ssh. 

As mentioned prior we need to take some additional security steps given that this server is running services that are externally facing. To fortify it we do several things, first we put Icarus on the Isolated subnet meaning that even if the device is somehow compromised it doesn't threaten other devices on the network. Second we use [Fail2Ban](https://www.fail2ban.org/wiki/index.php/Main_Page) to generate `iptable` entries that will block brute-force attempts against ssh and block access to all ports except that one we allow _(NAT already provides protection against this as a freebie)_. As with Daedalus authentication can only be done using keys. For web services we avoid port forwarding to them all together. What we instead do is reverse proxy using a Linode server in New Jersey called Aesop. Aesop also has hardened security, using the guide provided by Linode. The way we reverse proxy is using a technology called [Wireguard](https://www.wireguard.com/), for more details about it I suggest reading this [basic](https://eugene.kovalev.systems/blog/setting-up-wireguard) setup guide and more [advanced](https://eugene.kovalev.systems/blog/more-advanced-wireguard-setup) guide. Essentially the way it works is there is an `wg0` interface on Icarus that tunnels traffic from and to Aesop. On Aesop there is an instance of [Caddy](https://caddyserver.com/), a web server, which proxies all requests from it to Icarus over `wg0` to the port on which `lighttpd` is running on. A nice feature of `caddy` is automatic certificate renewal. This also gets around the ISPs measure like blocking ports `80` or `443`. 

Again, as all my other devices Icarus is a Syncthing node as well.

##### Helm

Helm is private email server that I have on the Isolated subnet. While the device runs Linux it's not a device to which there is an ssh interface as such I keep it separate from my other devices. The setup of Helm is fairly standard and straight forward, refer to the following article to get more details about it. My setup isn't different from what was described in the article.

##### Local CA

The last piece worth mentioning is the Local Certificate Authority which lives on my primary Linux desktop. I use a tool called `easy-rsa` to generate certificates for my OpenVPN server as well as local `lighttpd` servers (like the one pihole is running on). This is useful for getting `https` on LAN only services.

### Conclusion

This is the general outline of network infrastructure, security policies, and main system tools. There are plenty of other interesting details about setup of particular services or use of tools that I haven't mentioned here. These are saved for future posts as this post would otherwise be exceedingly long. There are plenty of improvements that can be made to network structure and methods around system administration. For example use of configuration deployment tools like _ansible_ to help with monthly maintenance and updates. Practices and standards around system management is an ever evolving thing and one I keep up with as time goes on. Even though all of this may seem like a lot it's important to remember that none of this was setup in a single afternoon. These were slow piece wise additions that will continue to occur as I learn better approaches to things. 
